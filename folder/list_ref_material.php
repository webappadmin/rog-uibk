<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/staff_auth.inc';
require '../include/errors.inc';
require_once '../classes/moduleutils.class.php';

$modID = check_var('module', 'GET', true, false, true);

$module_code = module_utils::get_moduleid_from_id($modID, $mysqli);

if (!$module_code) {
   $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
   $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Rog&#333;: <?php echo $string['referencematerial'] . ' ' . $configObject->get('cfg_install_type'); ?></title>
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <style type="text/css">
		.l {cursor:pointer}
  </style>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
  <script language="javascript">
    function selRef(divID, evt) {
      tmp_ID = $('#oldID').val();
      if (tmp_ID != '') {
        $('#' + tmp_ID).css('background-color', 'white');
        $('#' + tmp_ID).css('color', 'black');
      }

      $('#menu1a').hide();
      $('#menu1b').show();

      $('#oldID').val(divID);
      $('#divID').val(divID);

      $('#' + divID).css('background-color', '#B3C8E8');
      evt.cancelBubble = true;
    }

    function deselRef() {
      tmp_ID = $('#oldID').val();
      if (tmp_ID != '') {
        $('#' + tmp_ID).css('background-color', 'white');
      }
      $('#oldID').val('');
      $('#menu1b').hide();
      $('#menu1a').show();
    }

    function lon(lineID) {
      if (lineID != $('#oldID').val()) {
        $('#' + lineID).css('background-color', '#EEEEEE');
      }
    }

    function loff(lineID) {
      if (lineID != $('#oldID').val()) {
        $('#' + lineID).css('background-color', '');
      }
    }
    
    function editReference() {
      window.location="<?php echo $configObject->get('cfg_root_path') ?>/folder/edit_ref_material.php?refID=" + $('#divID').val() + "&module=<?php echo $_GET['module']; ?>";
    }
    
    function deleteReference() {
      notice=window.open("<?php echo $configObject->get('cfg_root_path') ?>/delete/check_delete_ref_material.php?refID=" + $('#divID').val() + "&module=<?php echo $_GET['module']; ?>","notice","width=420,height=170,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
      notice.moveTo(screen.width/2-210,screen.height/2-85);
      if (window.focus) {
        notice.focus();
      }
    }
  </script>
</head>

<body onclick="deselRef()">
<?php
  require '../include/toprightmenu.inc';
	
	echo draw_toprightmenu(296);

  $reference_materials = array();

  $result = $mysqli->prepare("SELECT reference_material.id, reference_material.title FROM reference_material, reference_modules WHERE reference_material.id = reference_modules.refID AND reference_material.deleted IS NULL AND idMod = ? ORDER BY reference_material.id");
  $result->bind_param('i', $_GET['module']);
  $result->execute();
  $result->store_result();
  $result->bind_result($id, $title);
  while ($result->fetch()) {
    $sub_result = $mysqli->prepare("SELECT moduleid FROM reference_modules, modules WHERE reference_modules.idMod = modules.id AND refID = ?");
    $sub_result->bind_param('i', $id);
    $sub_result->execute();
    $sub_result->store_result();
    $sub_result->bind_result($moduleid);
    while ($sub_result->fetch()) {
      if (isset($reference_materials[$id]['modules'])) {
        $reference_materials[$id]['modules'] .= ', ' . $moduleid;
      } else {
        $reference_materials[$id]['modules'] = $moduleid;
      }
    }
    $sub_result->close();
    
    $reference_materials[$id]['title'] = $title;
  }
  $result->close();

  require '../include/reference_material_options.inc';
?>
<div id="content" class="content">

<table class="header">
<tr>
<?php
  echo "<th><div class=\"breadcrumb\"><a href=\"../staff/index.php\">" . $string['home'] . "</a>&nbsp;&nbsp;<img src=\"../artwork/breadcrumb_arrow.png\" width=\"4\" height=\"7\" alt=\"-\" />&nbsp;&nbsp;<a href=\"./details.php?module=" . $_GET['module'] . "\">" . module_utils::get_moduleid_from_id($_GET['module'], $mysqli) . "</a></div><div style=\"margin-left:10px; font-size:200%; font-weight:bold\">" . $string['referencematerial'] . "</th>\n";
?>
<th style="text-align:right; vertical-align:top"><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon"></th>
</tr>

<?php
foreach ($reference_materials as $id => $details) {
  echo "<tr id=\"$id\" onclick=\"selRef($id,event)\" ondblclick=\"editReference($id)\" onmouseover=\"lon($id)\" onmouseout=\"loff($id)\" class=\"l\"><td><img src=\"/artwork/ref_16.png\" width=\"16\" height=\"16\" alt=\"\">&nbsp;" . $details['title'] . "</td><td>" . $details['modules'] . "</td></tr>\n";
}
echo "</table>\n";
$mysqli->close();
?>

</div>

</body>
</html>