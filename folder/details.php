<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
*
* Displays a list of papers.
*
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require_once '../include/staff_auth.inc';
require_once '../include/icon_display.inc';
require_once '../include/sidebar_menu.inc';
require_once '../include/errors.inc';
require_once '../include/demo_replace.inc';

require_once '../classes/moduleutils.class.php';
require_once '../classes/folderutils.class.php';
require_once '../classes/stateutils.class.php';
require_once '../classes/paperutils.class.php';

function getLastFolder($path) {
  $parts = explode(';' , $path);
  $part_no = count($parts);

  if ($part_no > 0) {
    return $parts[$part_no-1];
  } else {
    return $parts[0];
  }
}

$state = $stateutil->getState($userObject->get_user_ID(), $mysqli);

$folder_name = '';
$folder_type = '';
$file_no = 0;
$parent_list = array();
$add_member = false;

// Folder security checks
if (isset($_GET['folder'])) {
  $folder = $_GET['folder'];
} else {
  $folder = '';
}
if ($folder != '') {
  $orig_folder_name = '';

  $result = $mysqli->prepare("SELECT ownerID, name FROM folders WHERE id = ?");
  $result->bind_param('i', $folder);
  $result->execute();
  $result->store_result();
  $result->bind_result($folder_ownerID, $orig_folder_name);
  $result->fetch();
  $result->close();
  
  if ($orig_folder_name == '') {
    $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
    $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
  }

  $parent_list = array();
  if (substr_count($orig_folder_name, ';') > 0) {
    $last_semicolon = strrpos($orig_folder_name, ';');
    $path = substr($orig_folder_name, 0, $last_semicolon);
    $parts = explode(';', $path);
    $part_sql = '';
    foreach ($parts as $part) {
      if ($part_sql == '') {
        $part_sql = $part;
      } else {
        $part_sql .= ';' . $part;
      }
      $parent_results = $mysqli->prepare("SELECT id, name FROM folders WHERE name = ? AND ownerID = ? LIMIT 1");
      $parent_results->bind_param('si', $part_sql, $userObject->get_user_ID());
      $parent_results->execute();
      $parent_results->bind_result($parent_id, $parent_name);
      $parent_results->fetch();
      $parent_results->close();
      $parent_list[$parent_id] = $parent_name;
    }
  }
}

if (isset($_GET['module']) and $_GET['module'] != '') {
  $module = $_GET['module'];
  
  $module_details = module_utils::get_full_details_by_ID($_GET['module'], $mysqli);
    
  if (!$module_details) {
   $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
   $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
  } elseif ($module_details['active'] == 0) {
   $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
   $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);	
	}
	
} else {
  $module = '';
}

if (isset($_POST['submit'])) {
  $folder_results = $mysqli->prepare("SELECT name FROM folders WHERE id = ? LIMIT 1");
  $folder_results->bind_param('i', $folder);
  $folder_results->execute();
  $folder_results->bind_result($folder_parent);
  $folder_results->fetch();
  $folder_results->close();

  $new_folder_name = $folder_parent . ';' . $_POST['folder_name'];

  $duplicate_folder = folder_utils::folder_exists($new_folder_name, $userObject, $mysqli);
  if ($duplicate_folder == false) {
    folder_utils::create_folder($new_folder_name, $userObject, $mysqli);
  }
}

if ($folder != '') {
  $folders_array = explode(';', $orig_folder_name);
  $parts = count($folders_array) - 1;
  $selfenrol = 0;
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />

  <title>Rog&#333;<?php echo ' ' . $configObject->get('cfg_install_type'); ?></title>

  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <style type="text/css">
  <?php
  if (isset($state['showretired']) and $state['showretired'] == 'true') {
    echo ".retired {display:block}\n";
  } else {
    echo ".retired {display:none}\n";
  }
  ?>
	</style>

  <?php echo $configObject->get('cfg_js_root') ?>
  <script type="text/javascript" src="../js/sidebar.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/state.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
  <script language="JavaScript">
    function addQuestion(qType) {
      top.location.href='../question/edit/?type=' + qType + 'folder=<?php if (isset($_GET['folder'])) echo $_GET['folder']; ?>&module=<?php if (isset($_GET['module'])) echo $_GET['module']; ?>';
    }

    function deleteFolder() {
      notice=window.open("../delete/check_delete_folder.php?folderID=<?php if (isset($_GET['folder'])) echo $_GET['folder']; ?>","notice","width=420,height=170,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
      notice.moveTo(screen.width/2-210,screen.height/2-85);
      if (window.focus) {
        notice.focus();
      }
    }

    function folderProperties() {
      notice=window.open("properties.php?folder=<?php if (isset($_GET['folder'])) echo $_GET['folder']; ?>","properties","width=600,height=600,left="+(screen.width/2-300)+",top="+(screen.height/2-300)+",scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
      if (window.focus) {
        notice.focus();
      }
    }

    function newPaper(paperID) {
      notice = window.open("../paper/new_paper1.php?module=<?php if (isset($_GET['module'])) echo $_GET['module']; ?>&folder=<?php if (isset($_GET['folder'])) echo $_GET['folder']; ?>","properties","width=700,height=500,left="+(screen.width/2-325)+",top="+(screen.height/2-250)+",scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
      if (window.focus) {
        notice.focus();
      }
    }

    function addTeamMember() {
      notice = window.open("edit_team_popup.php?module=<?php if (isset($_GET['module'])) echo $_GET['module']; ?>&calling=paper_list&folder=<?php if (isset($_GET['folder'])) echo $_GET['folder']; ?>","properties","width=450,height="+(screen.height-200)+",left="+(screen.width/2-325)+",top=10,scrollbars=no,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
      if (window.focus) {
        notice.focus();
      }
    }

    function refreshPage() {
      $('.retired').toggle();
    }
  </script>
</head>

<body onclick="hideMenus()">
<?php
  require '../include/folder_options.inc';
  require '../include/toprightmenu.inc';

	echo draw_toprightmenu();
?>
<div id="content" class="content">
<form name="myform" action="<?php echo $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']; ?>" method="post">
<table class="header">
<?php
echo '<tr><th><div class="breadcrumb"><a href="../staff/index.php">' . $string['home'] . '</a>';
if (count($parent_list) > 0) {
  foreach ($parent_list as $parent_id=>$parent_name) {
    echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="details.php?folder=' . $parent_id . '">' . getLastFolder($parent_name) . '</a>';
  }
}
echo "</div></th><th style=\"text-align:right; vertical-align:top\"><img src=\"../artwork/toprightmenu.gif\" id=\"toprightmenu_icon\"></th></tr>\n";

echo '<tr><th><div style="margin-left:10px; font-size:200%; font-weight:bold">';
if ($folder != '') {
  echo $folders_array[$parts];
} elseif ($_GET['module'] != '') {
  echo $module_details['moduleid'] . ': <span style="font-weight:normal">' . $module_details['fullname'] . '</span>';
}
echo '</div></th>';
echo "<th style=\"text-align:right; vertical-align:top; padding-top:2px; padding-right:6px\"><input class=\"chk\" type=\"checkbox\" name=\"showretired\" id=\"showretired\" value=\"on\" onclick=\"refreshPage();\"";
if (isset($state['showretired']) and $state['showretired'] == 'true') echo ' checked="checked"';
echo " /> " . $string['showretired'] . "</th></tr>\n";

echo "</table>\n<br />\n";

$display_papers = true;
// Get members of current folder.
if (isset($_GET['module']) and $_GET['module'] != '') {

  $member_details = $mysqli->prepare("SELECT DISTINCT surname, initials, title, users.id FROM (modules_staff, users, modules) WHERE modules_staff.idMod = modules.id AND modules_staff.memberID = users.id AND modules.id = ? ORDER BY surname, initials");
  $member_details->bind_param('s', $module);
  $member_details->execute();
  $member_details->store_result();
  $member_details->bind_result($surname, $initials, $title, $tmp_userID);

  $tmp_html = '';
  if ($userObject->has_role('Demo')) {
    $i = 0;
  }
  if ($member_details->num_rows > 0) $tmp_html = '<ul type="square" style="line-height:155%; font-size:90%; color:#C0C0C0; margin-top:4px; margin-bottom:4px; margin-left:20px; padding-left:0px">';
  while ($member_details->fetch()) {
    if ($userObject->has_role('Demo')) {
      $tmp_html .= "<li><span style=\"color:#254280\">" . demo_replace_name($i) . "</span></li>\n";
      $i++;
    } elseif ($userObject->has_role(array('SysAdmin', 'Admin'))) {
      $tmp_html .= "<li><a style=\"color:#254280\" href=\"../users/details.php?userID=$tmp_userID&module=" . $_GET['module'] . "\">$surname, $initials. " . str_replace('Professor','Prof',$title) . "</a></li>\n";
    } else {
      $tmp_html .= "<li><span style=\"color:#254280\">$surname, $initials. " . str_replace('Professor','Prof',$title) . "</span></li>\n";
    }
    if ($tmp_userID == $userObject->get_user_ID() and $module_details['add_team_members'] == 1) $add_member = true;
  }
  if ($member_details->num_rows > 0) $tmp_html .= '</ul>';
  echo '<div style="float:right; width:165px; margin-right:10px; border:1px solid #C0C0C0">';
  if ($add_member == true or $userObject->has_role(array('SysAdmin', 'Admin'))) {
    echo '<div style="float:left; width:95%; padding:4px; background-color:#EBF3FF"><div style="float:left"><a href="" style="color:#254280" onclick="addTeamMember(); return false;" class="recent">' . $string['teammembers'] . '</a></div><div style="float:right"><a href="" onclick="addTeamMember(); return false;"><img src="../artwork/pencil_16.png" width="16" height="16" alt="' . $string['edit'] . '" border="0" /></a></div></div>';
  } else {
    echo '<div style="padding:4px; background-color:#EBF3FF">' . $string['teammembers'] . '</div>';
  }
  echo "<br clear=\"all\" />$tmp_html</div>\n";
  $member_details->close();
}

// Is it a self-enrol module.
if (isset($module_details['selfenroll']) and $module_details['selfenroll'] == 1) {
  $selfenrol_url = NetworkUtils::get_protocol() . $_SERVER['HTTP_HOST'] . $configObject->get('cfg_root_path') . '/self_enrol.php?moduleid=' . $module_details['moduleid'];
  echo "<div style=\"padding-left:10px\"><img src=\"../artwork/module_icon_16.png\" width=\"16\" height=\"16\" alt=\"modules\" /> <span style=\"color:#C00000\">" . $string['SelfenrolURL'] . ":</span> <a href=\"$selfenrol_url\">$selfenrol_url</a></div>\n<br />";
}

// Get any sub-folders first.
if ($folder != '') {
  $tmp_string = '';
  if (count($staff_modules) > 0) {
    $tmp_string = " OR idMod IN ('" . implode("','",array_keys($staff_modules)) . "')";
  }

  $tmp_folder_name = $orig_folder_name . ';%';
  $folder_details = $mysqli->prepare("SELECT folders.id, name, color FROM folders WHERE (ownerID=?) AND name LIKE ? AND deleted IS NULL ORDER BY name, folders.id");
  $folder_details->bind_param('is', $userObject->get_user_ID(), $tmp_folder_name);
  $folder_details->execute();
  $folder_details->bind_result($id, $name, $color);
  while ($folder_details->fetch()) {
    $display_name = str_replace("$orig_folder_name;","",$name);
    if (substr_count($display_name,';') == 0) {
      echo "<div class=\"f\" ><a href=\"../folder/details.php?folder=$id\" class=\"blacklink\"><img class=\"f_icon\" src=\"../artwork/" . $color . "_folder.png\" alt=\"Folder\" />$display_name</a></div>\n";
    }
  }
  $folder_details->close();
}

// New folder.
if (isset($_GET['newfolder']) and $_GET['newfolder'] == 'y' and !isset($_POST['submit'])) {
  echo "<div class=\"f\"><img class=\"f_icon\" src=\"../artwork/yellow_folder.png\" alt=\"Folder\" /><input type=\"text\" size=\"30\" name=\"folder_name\" value=\"" . $string['newfolder'] . "\" required onkeypress=\"if (event.keyCode == 59) illegalChar(event.keyCode);\" /><input type=\"submit\" name=\"submit\" value=\"" . $string['create'] . "\" /></div>\n<br clear=\"all\" />\n<br />";
}

// Get current owner papers.
if ($folder != '') {
  $query_string = "SELECT DISTINCT paper_ownerID, property_id, paper_type, MAX(screen) AS screens, paper_title, DATE_FORMAT(start_date,'%Y%m%d%H%i%s') AS start_date, DATE_FORMAT(start_date,'{$configObject->get('cfg_long_date_time')}') AS display_start_date, DATE_FORMAT(end_date,'{$configObject->get('cfg_long_date_time')}') AS display_end_date, exam_duration, title, initials, surname, retired, properties.password FROM (properties, users) LEFT JOIN papers ON properties.property_id=papers.paper WHERE properties.paper_ownerID=users.id AND folder=\"$folder\" AND deleted IS NULL GROUP BY paper_title ORDER BY paper_type, paper_title";
  $results = $mysqli->prepare($query_string);
} elseif ($_GET['module'] != '') {
  $paper_types = array();

  $results = $mysqli->prepare("SELECT DISTINCT paper_type, COUNT(paper_type) AS no_papers FROM properties, modules, properties_modules WHERE properties.property_id = properties_modules.property_id AND properties_modules.idMod = modules.id AND modules.id = ? AND deleted IS NULL GROUP BY paper_type");
  $results->bind_param('i', $_GET['module']);
  $results->execute();
  $results->bind_result($paper_type, $no_papers);
  while ($results->fetch()) {
    $paper_types[$paper_type] = $no_papers;
  }
  $results->close();
	// UPDATED sql query simplified removed the modules table as no data was coming from it.  also removed distinct as group by was doing it.  the user data is returned but for some reason the icons alt tags (that contain the user data don't display
  $query_string = "SELECT paper_ownerID, properties.property_id, paper_type, MAX(screen) AS screens, paper_title, DATE_FORMAT(start_date,'%Y%m%d%H%i%s') AS start_date, DATE_FORMAT(start_date,'{$configObject->get('cfg_long_date_time')}') AS display_start_date, DATE_FORMAT(end_date,'{$configObject->get('cfg_long_date_time')}') AS display_end_date, exam_duration, title, initials, surname, retired, properties.password FROM (properties, properties_modules, users) LEFT JOIN papers ON properties.property_id=papers.paper WHERE properties.property_id = properties_modules.property_id AND properties_modules.idMod = ? AND properties.paper_ownerID=users.id  AND deleted IS NULL GROUP BY paper_title ORDER BY paper_type, paper_title";
  $results = $mysqli->prepare($query_string);
  $results->bind_param('i', $_GET['module']);
}
$results->execute();
$results->bind_result($paper_ownerID, $property_id, $paper_type, $screens, $paper_title, $start_date, $display_start_date, $display_end_date, $exam_duration, $title, $initials, $surname, $retired, $password);
$results->store_result();
$old_p_type = '';
$sent_clear_all = false;
if ($display_papers) {
  if ($results->num_rows > 0) {
    while ($results->fetch()) {
      if ($old_p_type != $paper_type and (isset($_GET['module']) and $_GET['module'] != '') ) {
        if ($sent_clear_all) {
          echo "<br clear=\"left\" />";
        }
        $sent_clear_all = true;

        echo "<table border=\"0\" class=\"subsect\"><tr><td><nobr>" . $string[strtolower($types_array[$paper_type])] . " (" . $paper_types[$paper_type] . ")";
        if ($paper_type == 2) {
          echo "&nbsp;&nbsp;&nbsp;<span style=\"font-weight:normal\"><a href=\"../admin/calendar.php?module=" . $_GET['module'] . "#" . date("n") . "\"><img src=\"../artwork/shortcut_calendar_icon.png\" width=\"16\" height=\"16\" alt=\"Calendar\" /></a>&nbsp;<a href=\"../admin/calendar.php?module=" . $_GET['module'] . "#" . date("n") . "\">" . $string['calendar'] . "</a></span>\n";
        }
        echo "</nobr></td><td style=\"width:98%\"><hr noshade=\"noshade\" style=\"border:0px; height:1px; color:#E5E5E5; background-color:#E5E5E5; width:100%\" /></td></tr></table>\n";
        echo "<br />\n";
      }
      display_paper_icon($paper_ownerID, $property_id, $paper_type, $screens, $paper_title, $start_date, $display_start_date, $display_end_date, $exam_duration, $title, $initials, $surname, $retired, $password, $userObject);
      $old_p_type = $paper_type;
      $file_no++;
    }
    $results->close();
  }
}
$mysqli->close();
?>
</form>

</div>

</body>
</html>