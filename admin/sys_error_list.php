<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/sysadmin_auth.inc';
require_once '../classes/stateutils.class.php';

$state = $stateutil->getState($userObject->get_user_ID(), $mysqli);

?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  
  <title>Rog&#333;: <?php echo $string['systemerrrorreport'] . ' ' . $configObject->get('cfg_install_type'); ?></title>
  
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <link rel="stylesheet" type="text/css" href="../css/list.css" />
  <style type="text/css">
    .errl {padding-right:6px; vertical-align:top; text-align:right}
  </style>
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/state.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/list.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
  <script language="JavaScript">
    function refreshPage() {
      window.location = 'sys_error_list.php';
    }
    
    function openBug(lineID, evt) {
      selLine(lineID, evt);
      displayDetails();
    }
  </script>
</head>
<body>
<?php
  require '../include/sys_errors_menu.inc';
  require '../include/toprightmenu.inc';
	
	echo draw_toprightmenu();
?>
<div id="content" class="content">
<table class="header">
<tr>
<th colspan="4"><div class="breadcrumb"><a href="../staff/index.php"><?php echo $string['home']; ?></a>&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="./index.php"><?php echo $string['administrativetools']; ?></a></div><div style="margin-left:10px; font-size:200%; font-weight:bold"><?php echo $string['systemerrrorreport']; ?></th>
<th colspan="3" style="text-align:right; vertical-align:top"><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon"><br /><div style="padding-top:5px"><input class="chk" type="checkbox" name="showfixed" id="showfixed" value="1" onclick="refreshPage();"<?php if (isset($state['showfixed']) and $state['showfixed'] == 'true') echo ' checked="checked"'; ?> /> <?php echo $string['showfixed']; ?>&nbsp;</div></th>
</tr>
<tr><th><div class="col10"><?php echo $string['date']; ?></div></th><th class="vert_div"><?php echo $string['type']; ?></th><th class="vert_div"><?php echo $string['message']; ?></th><th class="vert_div"><?php echo $string['file']; ?></th><th class="vert_div"><?php echo $string['lineno']; ?></th><th class="vert_div"><?php echo $string['user']; ?></th><th class="vert_div"><?php echo $string['userid']; ?></th></tr>

<?php
  if (isset($state['showfixed']) and $state['showfixed'] == 'true') {
    $sql = "SELECT fixed, sys_errors.id, title, initials, surname, DATE_FORMAT(occurred,'{$configObject->get('cfg_long_date_time')}'), errtype, errstr, errfile, errline, users.id FROM sys_errors LEFT JOIN users ON users.id=sys_errors.userID ORDER BY sys_errors.id DESC LIMIT 1000";
  } else {
    $sql = "SELECT fixed, sys_errors.id, title, initials, surname, DATE_FORMAT(occurred,'{$configObject->get('cfg_long_date_time')}'), errtype, errstr, errfile, errline, users.id FROM sys_errors LEFT JOIN users ON users.id=sys_errors.userID WHERE fixed IS NULL ORDER BY sys_errors.id DESC LIMIT 1000";
  }

  $result = $mysqli->prepare($sql);
  $result->execute();
  $result->store_result();
  $result->bind_result($fixed, $errorID, $title, $initials, $surname, $occurred, $errtype, $errstr, $errfile, $errline, $tmp_userID);
  while ($result->fetch()) {
    if ($fixed == '') {
      echo "<tr class=\"l\" onclick=\"selLine($errorID,event)\" ondblclick=\"openBug($errorID,event)\" id=\"$errorID\"><td><div class=\"col\"><nobr>$occurred<nobr></div></td><td><div class=\"col\">$errtype</div></td><td><div class=\"col\">$errstr</div></td><td><div class=\"col\">$errfile</div></td><td><div class=\"errl\">$errline</div></td><td><div class=\"col\">$title&nbsp;$initials&nbsp;$surname</div></td><td><div class=\"col\">$tmp_userID</div></td></tr>\n";
    } else {
      echo "<tr class=\"l deleted\" onclick=\"selLine($errorID,event)\" ondblclick=\"openBug($errorID,event)\" id=\"$errorID\"><td><div class=\"col\"><nobr>$occurred</nobr></div></td><td><div class=\"col\">$errtype</div></td><td><div class=\"col\">$errstr</div></td><td><div class=\"col\">$errfile</div></td><td><div class=\"errl\">$errline</div></td><td><div class=\"col\">";
      if ($surname == '') {
        echo '<span class="grey">unauthenticated</span>';
      } else {
        echo "$title&nbsp;$initials&nbsp;$surname";
      }
      echo "</div></td><td><div class=\"col\">$tmp_userID</div>
      </td></tr>\n";
    }
  }
?>
</table>

</div>
</body>
</html>
