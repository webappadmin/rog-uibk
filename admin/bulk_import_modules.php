<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
*
* Bulk module creation
*
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/admin_auth.inc';
require_once '../classes/moduleutils.class.php';

ini_set("auto_detect_line_endings", true);

function returnTrueFalse($value) {
  $value = strtolower(trim($value));

  if ($value == 'yes' or $value == 'y' or $value == 'true') {
    return true;
  } else {
    return false;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  <title><?php echo $string['bulkmoduleimport'] . ' ' . $configObject->get('cfg_install_type'); ?></title>
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <style type="text/css">
    p {margin:0px; padding:0px}
    h1 {font-size:120%; font-weight:bold}
    label.error {display:block; color:#f00}
    .existing {color:#808080}
    .added {color:black}
    .failed {color:#C00000}
  </style>
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/jquery.validate.min.js"></script>
  <script type="text/javascript">
    $(function () { $('#import_form').validate(); });
  </script>
  </head>

  <body>
<?php
  require '../include/module_options.inc';
?>
<div id="content" class="content" style="font-size:80%">
<br />
<br />
<?php
  if (isset($_POST['submit'])) {
    if ($_FILES['csvfile']['name'] != 'none' and $_FILES['csvfile']['name'] != '') {
      if (!move_uploaded_file($_FILES['csvfile']['tmp_name'],  $configObject->get('cfg_tmpdir') . $userObject->get_user_ID() . "_module_create.csv"))  {
        echo uploadError($_FILES['csvfile']['error']);
        exit;
      } else {
        ?>
        <br /><br /><br />
        <div align="center">
        <table border="0" cellpadding="4" cellspacing="0" style="border:1px solid #95AEC8; font-size:120%">
        <tr>
        <td valign="middle" align="left" style="background-color:white"><img src="../artwork/upload_48.png" width="48" height="48" alt="Icon" />&nbsp;&nbsp;<span style="font-size:140%; font-weight:bold; color:#5582D2"><?php echo $string['bulkmoduleimport']; ?></span></td>
        </tr>
        <tr>
        <td align="left" style="background-color:#F1F5FB">
        <ul>

        <?php
        // Get a list of modules held by Rogo.
        $module_list = array();
        $result = $mysqli->prepare("SELECT DISTINCT moduleid FROM modules");
        $result->execute();
        $result->bind_result($moduleid);
        while ($result->fetch()) {
          $module_list[] = $moduleid;
        }
        $result->close();
        
        // Get a list of schools held by Rogo.
        $unknown_schoolID = 0;
        $school_list = array();
        $result = $mysqli->prepare("SELECT DISTINCT id, school FROM schools");
        $result->execute();
        $result->bind_result($school_id, $school_name);
        while ($result->fetch()) {
          $school_list[$school_name] = $school_id;
          if ($school_name == 'unknown') {
            $unknown_schoolID = $school_id;
          }
        }
        $result->close();

        $modulesAdded = 0;
        $lines = file( $configObject->get('cfg_tmpdir') . $userObject->get_user_ID() . "_module_create.csv");

        $students = array();
        foreach ($lines as $separate_line) {
          if (trim($separate_line) != '') {
            $fields = explode(',', $separate_line);
            
            if (trim($fields[0]) != 'Module ID' and trim($fields[0]) != 'ID') {  // Ignore header line
              $moduleid = trim($fields[0]);
              $fullname = trim($fields[1]);
              if (isset($school_list[trim($fields[2])])) {
                $schoolID = $school_list[trim($fields[2])];
              } else {
                if ($unknown_schoolID == 0) {
                  $result = $mysqli->prepare("SELECT id FROM faculty WHERE name='Administrative and Support Units' LIMIT 1");
                  $result->execute();
                  $result->bind_result($facultyID);
                  $result->fetch();
                  $result->close();

                  $unknown_schoolID = SchoolUtils::add_school($facultyID, '', $mysqli);
                }
                $schoolID = $unknown_schoolID;
              }              
              $sms_api = trim($fields[3]);
              $vle_api = trim($fields[4]);
              
              $peer = returnTrueFalse($fields[5]);
              $external = returnTrueFalse($fields[6]);
              $stdset = returnTrueFalse($fields[7]);
              $mapping = returnTrueFalse($fields[8]);
              $active = returnTrueFalse($fields[9]);
              $selfEnroll = returnTrueFalse($fields[10]);
              $neg_marking = returnTrueFalse($fields[11]);
              $ebel_grid_template = '';
                                 
              if (in_array($moduleid, $module_list)) {
                echo "<li class=\"existing\">$moduleid - " . $string['alreadyexists'] . "</li>\n";
              } else {
                $success = module_utils::add_modules($moduleid, $fullname, $active, $schoolID, $vle_api, $sms_api, $selfEnroll, $peer, $external, $stdset, $mapping, $neg_marking, $ebel_grid_template, $mysqli, 0, 0, 1, 1);
                if ($success) {
                  echo "<li class=\"added\">$moduleid - " . $string['added'] . "</li>\n";
                  $modulesAdded++;
                } else {
                  echo "<li class=\"fail\">$moduleid - " . $string['failed'] . "</li>\n";
                }
              }
            }
          }
        }
      }
    }
    unlink( $configObject->get('cfg_tmpdir') . $userObject->get_user_ID() . "_module_create.csv");

    echo "</ul>";
    echo "<div style=\"text-align:center\"><input type=\"button\" name=\"ok\" value=\"" . $string['ok'] . "\" onclick=\"window.location='list_modules.php'\" style=\"width:100px\" /></div>\n";

    $mysqli->close();
    ?>
    </div>
    </td>
    </tr>
    </table>
    </div>
    </td></tr>
    </table>
    <?php
  } else {
?>
<table border="0" cellpadding="4" cellspacing="0" style="width:650px; border:1px solid #95AEC8; margin-left:auto; margin-right:auto">
<tr>
<td style="width:56px; background-color:white"><img src="../artwork/upload_48.png" width="48" height="48" alt="Icon" /></td><td style="text-align:left; font-size:150%; font-weight:bold; color:#5582D2; width:90%"><?php echo $string['bulkmoduleimport']; ?></span></td>
</tr>
<tr>
<td align="left" style="background-color:#F1F5FB" colspan="2">

<p style="text-align:justify"><?php echo $string['msg1']; ?></p>
<blockquote>Module ID, Name, School, SMS API, Objectives API, Peer Review, External Examiners, Standards Setting, Mapping, Active, Allow Self-enrol, Negative Marking</blockquote>
<div style="text-align:center"><img src="../artwork/bulk_module_import_headings.png" width="891" height="59" alt="screenshot" border="1" /></div>
<br />
<div><?php echo $string['msg2']; ?></div>
<br />
<div align="center">
<form id="import_form" name="import" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
<p><strong><?php echo $string['csvfile']; ?></strong> <input type="file" size="50" name="csvfile" class="required" /></p>
<br />
<p><input type="submit" style="width:100px" value="<?php echo $string['import']; ?>" name="submit" />&nbsp;<input style="width:100px" type="button" value="<?php echo $string['cancel']; ?>" name="cancel" onclick="history.go(-1)" /></p>
</form>
</div>
</td>
</tr>
</table>

</div>
<?php
  }
?>
</body>
</html>