<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

  require '../include/sysadmin_auth.inc';
  require '../include/sidebar_menu.inc';
  require_once '../classes/networkutils.class.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
	
  <title>Rog&#333;: <?php echo $string['loglatedetails']; ?></title>
	
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <style type="text/css">
    .icon {padding-left:10px}
  </style>
  
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
</head>

<body>
<?php
  include '../include/admin_options.inc';
  require '../include/toprightmenu.inc';
	
	echo draw_toprightmenu();
?>

<div id="content" class="content" style="font-size:80%">

<table class="header">
<tr>
<th colspan="2"><div class="breadcrumb"><a href="../staff/index.php"><?php echo $string['home']; ?></a>&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="./index.php"><?php echo $string['administrativetools']; ?></a></div><div style="font-size:200%; margin-left:10px; font-weight:bold"><?php echo $string['loglatedetails']; ?></div></th>
<th style="text-align:right; vertical-align:top"><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon"></th>
</tr>
<tr>
<th></th>
<th><?php echo $string['papertitle']; ?></th>
<th style="width:50%"><?php echo $string['studentslate']; ?></th>
</tr>

<?php
  $icons = array('formative_16.gif', 'progress_16.gif', 'summative_16.gif');
  $data = array();

  $result = $mysqli->prepare("SELECT DISTINCT paper_type, paper_title, paperID, userID FROM log_metadata, log_late, properties, users WHERE log_late.metadataID = log_metadata.id AND log_metadata.paperID = properties.property_id AND log_metadata.userID = users.id AND roles LIKE '%Student%' GROUP BY userID ORDER BY paper_title");
  $result->execute();
  $result->bind_result($paper_type, $paper_title, $paperID, $uID);
  while ($result->fetch()) {
    $data[$paperID]['paper_title'] = $paper_title;
    $data[$paperID]['paper_type'] = $paper_type;
    $data[$paperID]['students'][] = $uID;
  }
  $result->close();
  
  
  foreach ($data as $paperID=>$row) {
    echo "<tr><td class=\"icon\"><a href=\"../paper/details.php?paperID=$paperID\"><img src=\"../artwork/" . $icons[$row['paper_type']] . "\" width=\"16\" height=\"16\" alt=\"\" /></a></td><td><a href=\"../paper/details.php?paperID=$paperID\">" . $row['paper_title'] . "</a></td><td>" . count($row['students']) . "</td></tr>";
  }
?>
</table>

</div>
</body>
</html>