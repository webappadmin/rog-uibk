<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/sysadmin_auth.inc';
require '../include/sort.inc';

?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  
  <title>Rog&#333;: <?php echo $string['courses'] . ' ' . $configObject->get('cfg_install_type'); ?></title>
  
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/submenu.css" />
  <link rel="stylesheet" type="text/css" href="../css/list.css" />
  
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/list.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
  <script language="javascript">
    function edit(courseID) {
      document.location.href='./edit_course.php?courseID=' + courseID;
    }
  </script>
</head>

<body>
<?php
  require '../include/course_options.inc';
  require '../include/toprightmenu.inc';
	
	echo draw_toprightmenu();
?>
<div id="content" class="content">

<table class="header">
<tr>
<th colspan="2"><div class="breadcrumb"><a href="../staff/index.php"><?php echo $string['home']; ?></a>&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="./index.php"><?php echo $string['administrativetools']; ?></a></div><div style="margin-left:10px; font-size:200%; font-weight:bold"><?php echo $string['courses']; ?></th>
<th style="text-align:right; vertical-align:top"><img src="../artwork/toprightmenu.gif" id="toprightmenu_icon"></th>
</tr>
<tr style="font-size:90%">
<?php
  if (isset($_GET['sortby'])) {
    $sortby = $_GET['sortby'];
    $ordering = $_GET['ordering'];
  } else {
    $sortby = 'code';
    $ordering = 'asc';
  }

  // output table header
  $table_order = array($string['code']=>'code', $string['name']=>'name', $string['school']=>'school');
  foreach($table_order as $display => $key) {
    if ($key == 'code') {
      echo "<th class=\"h vert_div col10\">";
    } else {
      echo "<th class=\"h vert_div\">";
    }
    if ($sortby == $key and $ordering == 'asc') {
      echo "<a href=\"" . $_SERVER['PHP_SELF'] . "?sortby=$key&ordering=desc\">$display</a>&nbsp;<img src=\"../artwork/desc.gif\" width=\"9\" height=\"7\" /></th>";
    } elseif ($sortby == $key and $ordering == 'desc') {
      echo "<a href=\"" . $_SERVER['PHP_SELF'] . "?sortby=$key&ordering=asc\">$display</a>&nbsp;<img src=\"../artwork/asc.gif\" width=\"9\" height=\"7\" /></th>";
    } else {
      echo "<a href=\"" . $_SERVER['PHP_SELF'] . "?sortby=$key&ordering=asc\">$display</a></th>";
    }
  }
  
?>
</tr>
<?php
$course_no = 0;
$courses = array();

$result = $mysqli->prepare("SELECT courses.id, school, name, description FROM courses LEFT JOIN schools ON courses.schoolid = schools.id WHERE name != 'left' AND name != 'none' AND courses.deleted IS NULL");
$result->execute();
$result->bind_result($id, $school, $name, $description);
while ($result->fetch()) {
  if ($school == '') $school = '<span style="color:#808080">unknown</span>';
  $courses[$course_no]['id'] = $id;
  $courses[$course_no]['code'] = $name;
  $courses[$course_no]['name'] = $description;
  $courses[$course_no]['school'] = $school;
  $course_no++;
}
$result->close();
$mysqli->close();

if (count($courses) > 0) {
  $courses = array_csort($courses, $sortby, $ordering);
}
$old_code_letter = '';
$old_name_letter = '';
$old_school = '';

for ($i=0; $i<$course_no; $i++) {
  $id = $courses[$i]['id'];
  
  if ($sortby == 'code' and strtoupper(substr($courses[$i]['code'], 0, 1)) != $old_code_letter) {
    echo '<tr><td colspan="3"><table border="0" class="subsect" style="margin-left:10px; padding-bottom:5px; width:99%"><tr><td>' . substr($courses[$i]['code'], 0, 1) . '</td><td style="width:98%"><hr noshade="noshade" style="border:0px; height:1px; color:#E5E5E5; background-color:#E5E5E5; width:100%" /></td></tr></table></td></tr>';
  } elseif ($sortby == 'name' and strtoupper(substr($courses[$i]['name'], 0, 1)) != $old_name_letter) {
    echo '<tr><td colspan="3"><table border="0" class="subsect" style="margin-left:10px; padding-bottom:5px; width:99%"><tr><td>' . substr($courses[$i]['name'], 0, 1) . '</td><td style="width:98%"><hr noshade="noshade" style="border:0px; height:1px; color:#E5E5E5; background-color:#E5E5E5; width:100%" /></td></tr></table></td></tr>';
  } elseif ($sortby == 'school' and $courses[$i]['school'] != $old_school) {
    echo '<tr><td colspan="3"><table border="0" class="subsect" style="margin-left:10px; padding-bottom:5px; width:99%"><tr><td><nobr>' . $courses[$i]['school'] . '</nobr></td><td style="width:98%"><hr noshade="noshade" style="border:0px; height:1px; color:#E5E5E5; background-color:#E5E5E5; width:100%" /></td></tr></table></td></tr>';
  }  
  echo "<tr id=\"$id\" onclick=\"selLine($id,event)\" ondblclick=\"edit($id)\" class=\"l\"><td class=\"col30\">" . $courses[$i]['code'] . "</td><td class=\"col\">" . $courses[$i]['name'] . "</td><td class=\"col\"><nobr>" . $courses[$i]['school'] . "</nobr></td></tr>\n";

  $old_code_letter = strtoupper(substr($courses[$i]['code'], 0, 1));
  $old_name_letter = strtoupper(substr($courses[$i]['name'], 0, 1));
  $old_school = $courses[$i]['school'];
}

?>
</table>
</div>

</body>
</html>