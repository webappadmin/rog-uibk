<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
*
* View internal and external reviewers comments
*
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/staff_auth.inc';
require '../include/media.inc';
require_once '../include/errors.inc';
require_once '../classes/moduleutils.class.php';
require_once '../classes/folderutils.class.php';

//HTML5 part
require_once '../lang/' . $language . '/question/edit/hotspot_correct.txt';
require_once '../lang/' . $language . '/question/edit/area.txt';
require_once '../lang/' . $language . '/paper/hotspot_answer.txt';
require_once '../lang/' . $language . '/paper/hotspot_question.txt';
require_once '../lang/' . $language . '/paper/label_answer.txt';
$jstring = $string; //to pass it to JavaScript HTML5 modules
//HTML5 part

$type = check_var('type', 'GET', true, false, true);
$paperID = check_var('paperID', 'GET', true, false, true);

function displayRank($rank_position, $string) {
  if ($rank_position == 1) {
    $html = '1st';
  } elseif ($rank_position == 2) {
    $html = '2nd';
  } elseif ($rank_position == 3) {
    $html = '3rd';
  } elseif ($rank_position == 9990) {
    $html = $string['na'];
  } else {
    $html = $rank_position . 'th';
  }
  return $html;
}

function displayComments($questionID, $comments_data, $qtype, $qno, $reviewer_data, $type, $string, $language) {

  $html = "<tr><td></td><td><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:98%\">\n";
  $html .= "<tr><td colspan=\"5\"><strong>" . $string[$type . 'comments'] . "$qno</strong>&nbsp;<img onclick=\"editQ($questionID, $qno)\" style=\"cursor:pointer\" src=\"../artwork/edit.png\" width=\"16\" height=\"16\" alt=\"" . $string['editquestion'] . "\" /></td></tr>\n";
  $html .= "<tr><td style=\"width:20px\"><div class=\"reviewbar\">&nbsp;</div></td><td style=\"width:20%\"><div class=\"reviewbar\">" . $string['reviewer'] . "</div></td><td style=\"width:35%\"><div class=\"reviewbar\">" . $string['comment'] . "</div></td><td style=\"width:10%\"><div class=\"reviewbar\">" . $string['action'] . "</div></td><td style=\"width:35%\"><div class=\"reviewbar\">" . $string['response'] . "</div></td></tr>\n";
  
  foreach ($reviewer_data as $reviewerID=>$rev_data) {
    $reviewer_name = $rev_data['title'] . ' ' . $rev_data['initials'] .  ' ' . $rev_data['surname'];
    $comment = '';
    if (isset($comments_data[$questionID][$reviewerID])) {
      $comment = nl2br($comments_data[$questionID][$reviewerID]['comment']);
    
      $image = 'ok_comment.png';
      $status = 'OK';
      if ($comments_data[$questionID][$reviewerID]['category'] == 2) {
        $image = 'minor_comment.png';
        $status = 'Minor';
      } elseif ($comments_data[$questionID][$reviewerID]['category'] == 3) {
        $image = 'major_comment.png';
        $status = 'Major';
      }
      if (trim($comment) == '') {
        $comment = '<span style="color:#808080">' . $string['nocomment'] . '</span>';
        $action = '<span style="color:#808080">' . $string['nocomment'] . '</span>';
        $response = '<span style="color:#808080">' . $string['na'] . '</span>';
      } else {
        $action = $string[$comments_data[$questionID][$reviewerID]['action']];
        $response = nl2br($comments_data[$questionID][$reviewerID]['response']);
      }
      $extra = '';
      $image = "<img src=\"../artwork/$image\" width=\"16\" height=\"16\" alt=\"$status\" />";
    } else {
      $comment = '';
      $action = $string['notreviewed'];
      $response = '';
      $extra = ' notreviewed';
      $image = '';
    }
    
    $html .= "<tr><td class=\"reviewline$extra\">$image</td><td class=\"reviewline$extra\">$reviewer_name</td><td class=\"reviewline$extra\">$comment</td><td class=\"reviewline$extra\">$action</td><td class=\"reviewline$extra\">$response</td></tr>\n";
  }
  
  $html .= "</table></td></tr>\n";

  return $html;
}

function displayQuestion($q_no, $q_id, $theme, $scenario, $leadin, $q_type, $correct, $q_media, $q_media_width, $q_media_height, $options, $comments, $correct_buf, $display_method, $score_method, $labelcolor, $themecolor, $std, $reviewer_data, $type, $string, $language) {
  $configObject = Config::get_instance();

  $cfg_root_path = $configObject->get('cfg_root_path');

  if ($theme != '') echo "<tr><td colspan=\"2\"><h1 style=\"color:$themecolor\">$theme</h1></td></tr>\n";
  echo "<tr>\n";

  if ($q_type != 'extmatch' and $q_type != 'matrix') {
    if ($q_type == 'info') {
      echo "<td colspan=\"2\" style=\"padding-left:10px; padding-right:10px\">$leadin\n";
    } else {
      if ($scenario != '') {
        echo "<tr><td class=\"q_no\">$q_no.&nbsp;</td><td>$scenario<br />\n";
        echo $leadin;
        if ($q_media != '' and $q_type != 'hotspot' and $q_type != 'labelling' and $q_type != 'area') {
          echo "<p align=\"center\">" . display_media($q_media, $q_media_width, $q_media_height, '') . "</p>\n";
        }
        if ($q_type != 'hotspot' and $q_type != 'labelling' and $q_type != 'blank') echo "<p>\n<table cellpadding=\"3\" cellspacing=\"0\" border=\"0\" style=\"margin-left:30px\">\n";
      } else {
        echo "<tr><td class=\"q_no\">$q_no.&nbsp;</td><td>$leadin\n";
        if ($q_media != '' and $q_type != 'hotspot' and $q_type != 'labelling' and $q_type != 'area') {
          echo "<p align=\"center\">" . display_media($q_media, $q_media_width, $q_media_height, '') . "</p>\n";
        }
        if ($q_type != 'hotspot' and $q_type != 'labelling' and $q_type != 'blank') echo "<p>\n<table cellpadding=\"3\" cellspacing=\"0\" border=\"0\" style=\"margin-left:30px\">\n";
      }
    }
    switch ($q_type) {
      case 'area':
      ?>
      <br />
			<?php
					if ($configObject->get('cfg_interactive_qs') == 'html5') {
					//<!-- ======================== HTML5 part include finf ================= -->
					echo "<canvas id='canvas" . $q_no . "' width='" . ($q_media_width + 2) . "' height='" . ($q_media_height + 1) . "'></canvas>\n";
					echo "<div style='width:100%;text-align: left;' id='canvasbox'></div>\n";
					echo "<script language='JavaScript' type='text/javascript'>\n";
					echo "setUpQuestion(" . $q_no . ", 'q" . $q_no . "','" . $language . "', '" . $q_media . "', '" . $correct . "', '','','#FFC0C0','area','script');\n";
					echo "</script>\n";
					//<!-- ==================================================== -->
				} else {
        	echo "<script language='javascript'>\n";
					echo "write_string('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" width=\"" . ($q_media_width + 2) . "\" height=\"" . ($q_media_height + 1) . "\" id=\"externalinterfaceq" . $q_no . "_1\" align=\"top\">');\n";
					echo "write_string('<param name=\"movie\" value=\"" . $configObject->get('cfg_root_path') . "/question/edit/area.swf\" />');\n";
					echo "write_string('<param name=\"quality\" value=\"high\" />');\n";
					echo "write_string('<param name=\"bgcolor\" value=\"#ffffff\" />');\n";
					echo "write_string('<param name=\"play\" value=\"true\" />');\n";
					echo "write_string('<param name=\"loop\" value=\"true\" />');\n";
					echo "write_string('<param name=\"wmode\" value=\"opaque\" />');\n";
					echo "write_string('<param name=\"scale\" value=\"showall\" />');\n";
					echo "write_string('<param name=\"menu\" value=\"true\" />');\n";
					echo "write_string('<param name=\"devicefont\" value=\"false\" />');\n";
					echo "write_string('<param name=\"salign\" value=\"top\" />');\n";
					echo "write_string('<param name=\"allowScriptAccess\" value=\"sameDomain\" />');\n";
					echo "write_string('<!--[if !IE]>-->');\n";
					echo "write_string('<object type=\"application/x-shockwave-flash\" data=\"" . $configObject->get('cfg_root_path') . "/question/edit/area.swf\" id=\"externalinterfaceq" . $q_no . "_2\" width=\"" . ($q_media_width + 2) . "\" height=\"" . ($q_media_height + 1) . "\">');\n";
					echo "write_string('<param name=\"movie\" value=\"" . $configObject->get('cfg_root_path') . "/question/edit/area.swf\" />');\n";
					echo "write_string('<param name=\"quality\" value=\"high\" />');\n";
					echo "write_string('<param name=\"bgcolor\" value=\"#ffffff\" />');\n";
					echo "write_string('<param name=\"play\" value=\"true\" />');\n";
					echo "write_string('<param name=\"loop\" value=\"true\" />');\n";
					echo "write_string('<param name=\"wmode\" value=\"opaque\" />');\n";
					echo "write_string('<param name=\"scale\" value=\"showall\" />');\n";
					echo "write_string('<param name=\"menu\" value=\"true\" />');\n";
					echo "write_string('<param name=\"devicefont\" value=\"false\" />');\n";
					echo "write_string('<param name=\"salign\" value=\"top\" />');\n";
					echo "write_string('<param name=\"allowScriptAccess\" value=\"sameDomain\" />');\n";
					echo "write_string('<!--<![endif]-->');\n";
					echo "write_string('<a href=\"https://www.adobe.com/go/getflash\"> <img src=\"https://www.adobe.com/images/shared/download_buttons/get_flash_player.gif\" alt=\"Get Adobe Flash player\" /></a>');\n";
					echo "write_string('<!--[if !IE]>-->');\n";
					echo "write_string('</object>');\n";
					echo "write_string('<!--<![endif]-->');\n";
					echo "write_string('</object>');\n";
          echo "sendTextToAS3('$language', 'q$q_no', 1, '../media/" . $q_media . "', '" . $correct . "', '');\n";
					echo "</script>\n<br />";
         }          
			?>
      <input type="hidden" name="q<?php echo $q_no; ?>" id="q<?php echo $q_no; ?>" />
      <?php
        break;
      case 'blank':
        $options[0] = preg_replace("| mark=\"([0-9]{1,3})\"|","",$options[0]);
        $options[0] = preg_replace("| size=\"([0-9]{1,3})\"|","",$options[0]);
        $blank_details = array();
        $blank_details = explode('[blank',$options[0]);
        $array_size = count($blank_details);
        $blank_count = 0;
        while ($blank_count < $array_size) {
          if (strpos($blank_details[$blank_count],'[/blank]') === false) {
            echo $blank_details[$blank_count];
          } else {
            $end_start_tag = strpos($blank_details[$blank_count],']');
            $start_end_tag = strpos($blank_details[$blank_count],'[/blank]');
            $blank_options = substr($blank_details[$blank_count],($end_start_tag+1),($start_end_tag-1));
            $remainder = substr($blank_details[$blank_count], ($start_end_tag+8));

            if ($display_method == 'dropdown') {
              echo '<select>';
              $options_array = array();
              $options_array = explode(',',$blank_options);
              $i = 0;
              foreach ($options_array as $individual_blank_option) {
                $individual_blank_option = trim($individual_blank_option);
                if ($i == 0) {
                  echo '<option value="" selected="selected">' . $individual_blank_option . '</option>';
                } else {
                  echo '<option value="">' . $individual_blank_option . '</option>';
                }
                $i++;
              }
              echo '</select>';
            } else {
              // Correct answer.
              $correct_options = explode(',' , $blank_options);
              echo '<input type="text" size="10" value="' . $correct_options[0] . '" />';
            }
            echo $remainder;
          }
          $blank_count++;
        }
        break;
      case 'calculation':
        break;
      case 'dichotomous':
        $tmp_std_array = explode(',', $std);
        $std_part = 0;
        if ($score_method == 'YN_Positive') {
          $true_label = 'Yes';
          $false_label = 'No';
        } else {
          $true_label = 'True';
          $false_label = 'False';
        }
        $i = 0;
        foreach ($options as $individual_option) {
          $i++;
          if ($correct_buf[$i-1] == 't') {
            echo "<tr><td style=\"font-weight:bold\">$true_label</td><td>$individual_option</td></tr>\n";
          } else {
            echo "<tr><td style=\"font-weight:bold\">$false_label</td><td>$individual_option</td></tr>\n";
          }
        }
        break;
      case 'labelling':
        $tmp_std_array = explode(',',$std);
        $std_part = 0;
        $tmp_std_array = explode(',',$std);
        $std_part = 0;
        $max_col1 = 0;
        $max_col2 = 0;
        $tmp_first_split = explode(';', $correct);
        $tmp_second_split = explode('|', $tmp_first_split[11]);
        foreach ($tmp_second_split as $ind_label) {
          $label_parts = explode('$', $ind_label);
          if (isset($label_parts[4]) and trim($label_parts[4]) != '') {
            if ($label_parts[0] < 10) {
              $max_col1 = $label_parts[0];
            } else {
              $max_col2 = $label_parts[0];
            }
          }
        }
        $max_col2-=10;

        $max_label = max($max_col1, $max_col2);

        $tmp_height = $q_media_height;
        if ($tmp_height < ($max_label * 55)) $tmp_height = ($max_label * 55);
        $correct = str_replace('"', '&#034;', $correct);
        $correct = str_replace("'", '&#039;', $correct);
?>
  <div align="center">
	<?php
	require_once '../classes/configobject.class.php';
	$configObject          = Config::get_instance();
	if ($configObject->get('cfg_interactive_qs') == 'html5') {
		//<!-- ======================== HTML5 part rep disc ================= -->
		echo "<canvas id='canvas" . $q_no . "' width='" . ($q_media_width + 220) . "' height='" . $tmp_height . "'></canvas>\n";
		echo "<br /><div style='width:100%;text-align: left;' id='canvasbox'></div>\n";
		echo "<script language='JavaScript' type='text/javascript'>\n";
		echo "setUpQuestion(" . $q_no . ", 'flash" . $q_no . "', '" . $language . "', '" . $q_media . "', '" . trim($correct) . "', '', '','#FFC0C0','labelling','analysis');\n";
		echo "</script>\n";
		//<!-- ==================================================== -->
	} else {
		echo "<script language='JavaScript'>\n";
		echo "function swfLoaded" . $q_no . "(message) {\n";
		echo "var num = message.substring(5,message.length);\n";
		echo "setUpFlash(num, message, '" . $language . "', '" . $q_media . "', '" . trim($correct) . "', '','#FFC0C0');}\n";
		echo "write_string('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" id=\"flash" . $q_no . "\" width=\"" . ($q_media_width + 250) . "\" height=\"" . $tmp_height . "\" align=\"middle\">');\n";
		echo "write_string('<param name=\"allowScriptAccess\" value=\"always\" />');\n";
		echo "write_string('<param name=\"movie\" value=\"" . $configObject->get('cfg_root_path') . "/reports/label_analysis.swf\" />');\n";
		echo "write_string('<param name=\"quality\" value=\"high\" />');\n";
		echo "write_string('<param name=\"bgcolor\" value=\"#ffffff\" />');\n";
		echo "write_string('<embed src=\"" . $configObject->get('cfg_root_path') . "/reports/label_analysis.swf\" quality=\"high\" bgcolor=\"#ffffff\" width=\"" . ($q_media_width + 250) . "\" height=\"" . $tmp_height . "\" swliveconnect=\"true\" id=\"flash" . $q_no . "\" name=\"flash" . $q_no . "\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\" pluginspage=\"https://www.macromedia.com/go/getflashplayer\" />');\n";
		echo "write_string('</object>');\n";
		echo "</script>\n";
	}
	?>
	</div>
  <br />
<?php
        break;
      case 'hotspot':
        $tmp_width = ($q_media_width + 301);
        if ($tmp_width < 375) $tmp_width = 375;
        $tmp_height = $q_media_height + 30;
        ?>
            <div>
						<?php
						if ($configObject->get('cfg_interactive_qs') == 'html5') {
							//"<!-- ======================== HTML5 part include finf ================= -->
							echo "<canvas id='canvas" . $q_no . "' width='" . $tmp_width . "' height='" . $tmp_height . "'></canvas>\n";
							echo "<br /><div style='width:100%;text-align: left;' id='canvasbox'></div>\n";
							echo "<script language='JavaScript' type='text/javascript'>\n";
							echo "setUpQuestion(" . $q_no . ", 'flash" . $q_no . "', '" . $language . "', '" . $q_media . "', '" . str_replace('&nbsp;', ' ', $correct) . "', '', '0,0,0000000000000','#FFC0C0','hotspot','script');\n";
							echo "</script>\n";
							//<!-- ==================================================== -->
						} else {
							echo "<script language='JavaScript'>\n";
							echo "function swfLoaded" . $q_no . "(message) {\n";
							echo "var num = message.substring(5,message.length);\n";
							echo "setUpFlash(num, message, '" . $language . "', '" . $q_media . "', '" . str_replace('&nbsp;', ' ', $correct) . "', '', '1,1,0000000000000','#FFC0C0');}\n";
							echo "write_string('<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"https://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" id=\"flash" . $q_no . "\" width=\"" . $tmp_width . "\" height=\"" . $tmp_height . "\" align=\"middle\">');\n";
							echo "write_string('<param name=\"allowScriptAccess\" value=\"always\" />');\n";
							echo "write_string('<param name=\"movie\" value=\"" . $configObject->get('cfg_root_path') . "/paper/hotspot_answer.swf\" />');\n";
							echo "write_string('<param name=\"quality\" value=\"high\" />');\n";
							echo "write_string('<param name=\"bgcolor\" value=\"" . $bgcolor . "\" />');\n";
							echo "write_string('<embed src=\"" . $configObject->get('cfg_root_path') . "/paper/hotspot_answer.swf\" quality=\"high\" bgcolor=\"white\" width=\"" . $tmp_width . "\" height=\"" . $tmp_height . "\" swliveconnect=\"true\" id=\"flash" . $q_no . "\" name=\"flash" . $q_no . "\" align=\"middle\" allowScriptAccess=\"always\" type=\"application/x-shockwave-flash\" pluginspage=\"https://www.macromedia.com/go/getflashplayer\" />');\n";
							echo "write_string('</object>');\n";
							echo "</script>\n";
						}
						?>
						</div>
						<?php
        break;
      case 'mcq':
        $i = 0;
        foreach ($options as $individual_option) {
          $i++;
          if ($correct == $i) {
            echo "<tr><td><input type=\"radio\" checked=\"checked\" /></td><td>$individual_option</td></tr>\n";
          } else {
            echo "<tr><td><input type=\"radio\" /></td><td>$individual_option</td></tr>\n";
          }
        }
        break;
      case 'true_false':
        if ($correct == 't') {
          echo "<tr><td><input type=\"radio\" checked=\"checked\" /></td><td>True</td></tr>\n";
          echo "<tr><td><input type=\"radio\" /></td><td>False</td></tr>\n";
        } else {
          echo "<tr><td><input type=\"radio\" /></td><td>True</td></tr>\n";
          echo "<tr><td><input type=\"radio\" checked=\"checked\" /></td><td>False</td></tr>\n";
        }
        break;
      case 'mrq':
        $tmp_std_array = explode(',',$std);
        $i = 0;
        $correct_stems = 0;
        foreach ($options as $individual_option) {
          $i++;
          if ($correct_buf[$i-1] == 'y') {
            echo "<tr><td><input type=\"checkbox\" checked=\"checked\" /></td><td>$individual_option</td></tr>\n";
          } else {
            echo "<tr><td><input type=\"checkbox\" /></td><td>$individual_option</td></tr>\n";
          }
        }
        break;
      case 'rank':
        $tmp_std_array = explode(',', $std);
        $std_part = 0;
        $rank_no = 0;
        foreach ($correct_buf as $individual_correct) {
          if ($individual_correct > $rank_no and $individual_correct < 9990) $rank_no = $individual_correct;
        }

        $i = 0;
        foreach ($options as $individual_option) {
          $i++;
          echo "<tr><td><select><option value=\"\"></option>";
          for ($a=1; $a<=$rank_no; $a++) {
            if ($correct_buf[$i-1] == $a) {
              echo '<option value="" selected="selected">' . displayRank($a, $string) . '</option>';
            } else {
              echo '<option value="">' . displayRank($a, $string) . '</option>';
            }
          }
          echo "</select></td><td>$individual_option</td></tr>\n";
        }
        break;
      case 'textbox':
        $correct_answers = explode(';', $correct);
        foreach ($correct_answers as $single_answer) {
          $answer_count[$single_answer] = 0;
        }
        break;
    }
    if ($q_type != 'info' and $q_type != 'blank' and $q_type != 'labelling' and $q_type != 'hotspot') echo "</table></p>\n";
  } elseif ($q_type == 'matrix') {
    $matching_scenarios = explode('|', $scenario);
    $correct_answers = explode('|', $correct);
    echo "<tr><td class=\"q_no\">$q_no.&nbsp;</td><td>$leadin\n";
    echo '<ol type="i">';
    $i = 0;
    echo '<table cellpadding="2" cellspacing="0" border="1" class="matrix">';
    echo "<tr>\n<td colspan=\"2\">&nbsp;</td>";
    foreach ($options as $single_option) {
      echo '<td>' . $single_option . '</td>';
    }

    echo "<tr>\n";

    $row_no = 0;
    foreach ($matching_scenarios as $single_scenario) {
      if (trim($single_scenario) != '') {
        echo "<tr>\n";
        echo '<td align="right">' . chr(65 + $row_no) . '.</td><td>' . $single_scenario . '</td>';
        $answer_no = 1;
        $col_no = 1;
        foreach ($options as $single_option) {
          if ($correct_answers[$row_no] == $col_no) {
            echo '<td><div align="center"><input type="radio" name="q' . $q_no . '_' . $row_no . '" value="' . $answer_no . '" checked /></div></td>';
          } else {
            echo '<td><div align="center"><input type="radio" name="q' . $q_no . '_' . $row_no . '" value="' . $answer_no . '" /></div></td>';
          }
          $answer_no++;
          $col_no++;
        }
        echo "</tr>\n";
        $row_no++;
      }
    }
    echo '</table>';
    echo "</ol>\n</td></tr>\n";
  } elseif ($q_type == 'extmatch') {
    $matching_scenarios = explode('|', $scenario);
    $matching_media = explode('|', $q_media);
    $tmp_media_width_array = explode('|',$q_media_width);
    $tmp_media_height_array = explode('|',$q_media_height);
    $tmp_answers_array = explode('|',$correct_buf[0]);
    $tmp_std_array = explode(',',$std);
    $std_part = 0;

    array_unshift($matching_scenarios, '');
    $max_scenarios = max(count($matching_scenarios), count($matching_media));
    $scenario_no = 0;
    for ($part_id = 1; $part_id < $max_scenarios; $part_id++) {
      if ((isset($matching_scenarios[$part_id]) and trim(strip_tags($matching_scenarios[$part_id],'<img>')) != '')
              or (isset($matching_media[$part_id]) and $matching_media[$part_id] != '')) {
        $scenario_no++;
      }
    }

    echo "<tr><td class=\"q_no\">$q_no.&nbsp;</td><td>$leadin\n<ol type=\"A\">";
    if ($matching_media[0] != '') {
      echo "<div align=\"center\">" . display_media($matching_media[0], $tmp_media_width_array[0], $tmp_media_height_array[0], '') . "</div>\n";
    }
    for ($i=1; $i<=$scenario_no; $i++) {
      echo "<li>\n";
      if (isset($matching_media[$i]) and $matching_media[$i] != '') {
        echo "<div>" . display_media($matching_media[$i], $tmp_media_width_array[$i], $tmp_media_height_array[$i], '') . "</div>\n";
      }
      if ($matching_scenarios[$i]) echo $matching_scenarios[$i] . '<br />';
      $option_no = 1;
      $specific_answers = array();
      $specific_answers = explode('$', $tmp_answers_array[$i-1]);
      if (count($specific_answers) > 1) {
        echo '<select multiple="multiple" size="10">';
      } else {
        echo '<select>';
      }
      foreach ($options as $individual_option) {
        $answer_match = false;
        for ($x=0; $x<count($specific_answers); $x++) {
          if ($option_no == $specific_answers[$x]) $answer_match = true;
        }
        if ($answer_match == true) {
          echo "<option value=\"\" selected=\"selected\">$individual_option</option>\n";
        } else {
          echo "<option value=\"\">$individual_option</option>\n";
        }
        $option_no++;
      }
      echo "</select><br />&nbsp;</li>\n";
    }
    echo "</ol>\n";
  }
  echo "</td></tr>\n";

  // Display comments here.
  if ($q_type != 'info') echo displayComments($q_id, $comments, $q_type, $q_no, $reviewer_data, $type, $string, $language);
  echo "<tr><td colspan=\"2\">&nbsp;</td></tr>\n";
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />

  <title>Rog&#333;: <?php echo ucfirst($type); ?> Comments Report</title>

  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/start.css" />
  <link rel="stylesheet" type="text/css" href="../css/warnings.css" />
  <style type="text/css">
    table {font-size:100%;table-layout:auto}
    h1 {margin-left:15px; font-size:18pt}
    p {margin-left:0px; margin-right:15px; margin-top:0px; padding-top:0px}
    .figures {text-align:right}
    .q_no {text-align:right; vertical-align:top; width:50px}
    .grey {color:#808080}
    .OK {}
    .Minor {}
    .Major {}
    .notreviewed {color:#C00000}
    .screenbrk {
      color:#15428B;
      font-weight:bold;
      font-size:90%;
      height:70px;
      width:100%;
      border-top: 1px solid #B5C4DF;
      background: -moz-linear-gradient(top, #E4EEFC, #FFFFFF);
      background: -webkit-linear-gradient(top, #E4EEFC, #FFFFFF);
      background-image: -ms-linear-gradient(top, #E4EEFC 0%, #FFFFFF 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#E4EEFC', endColorstr='#FFFFFF');
    }
    .reviewbar {
		  color:white;
			background-color:#295AAD;
			width:100%;
      padding:2px;
		}
    .reviewline {
      padding:2px;
      border-bottom:solid 1px #C0C0C0;
    }
  </style>

  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../tools/mee/mee/js/mee_src.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/flash_include.js"></script>
  <script type="text/javascript" src="../js/jquery.flash_q.js"></script>
  <script type="text/javascript" src="../js/ie_fix.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
	<!-- HTML5 part start -->
	<script type='text/javascript'><?php echo "var lang_string = ".  json_encode($jstring) . ";\n";?></script>
	<script type="text/javascript" src="../js/html5.images.js"></script>
	<script type="text/javascript" src="../js/qsharedf.js"></script>
	<script type="text/javascript" src="../js/qlabelling.js"></script>
	<script type="text/javascript" src="../js/qhotspot.js"></script>
	<script type="text/javascript" src="../js/qarea.js"></script>
	<!-- HTML5 part end -->

	
  <script language="JavaScript">
    function getScrollXY() {
      document.getElementById('scrOfY').value = $('body,html').scrollTop();
    }

    function editQ(qid, qno) {
      location.href='../question/edit/index.php?q_id=' + qid + '&qNo=' + qno + '&paperID=<?php echo $_GET['paperID']; ?>&folder=<?php echo $_GET['folder']; ?>&module=<?php echo $_GET['module']; ?>&calling=<?php echo $type; ?>_comments&scrOfY=' + document.getElementById('scrOfY').value + '&tab=comments';
    }

    function showHideSerious() {
      if (document.styleSheets[0].rules) {
        document.styleSheets[0].rules.item(7).style.display = (document.styleSheets[0].rules.item(7).style.display == 'none') ? 'block' : 'none';
      } else {
        document.styleSheets[0].cssRules[7].style.display = (document.styleSheets[0].cssRules[7].style.display == 'none') ? 'table-row' : 'none';
      }
    }
  </script>
</head>

<body onscroll="getScrollXY()"<?php
if (isset($_GET['scrOfY'])) {
  if ($_GET['scrOfY'] > 0) echo ' onload="window.scrollTo(0,' . $_GET['scrOfY'] . ')"';
}
?>>
<div id="maincontent">
<form name="theform">
<table class="header">
<?php
  require '../include/toprightmenu.inc';
	
	echo draw_toprightmenu(30);
	
  $comments_array = array();
  
  // Get some paper properties
  $result = $mysqli->prepare("SELECT paper_type, paper_title, marking, pass_mark FROM properties WHERE property_id = ?");
  $result->bind_param('i', $paperID);
  $result->execute();
  $result->bind_result($paper_type, $paper, $marking, $pass_mark);
  $result->fetch();
  $result->close();
  
  if (!isset($paper)) {
    $msg = sprintf($string['furtherassistance'], $configObject->get('support_email'), $configObject->get('support_email'));
    $notice->display_notice_and_exit($mysqli, $string['pagenotfound'], $msg, $string['pagenotfound'], '../artwork/page_not_found.png', '#C00000', true, true);
  }
  
  
  $reviewer_data = array();
  $result = $mysqli->prepare("SELECT users.id, title, initials, surname FROM properties_reviewers, users WHERE properties_reviewers.reviewerID = users.id AND type = ? AND paperID = ? ORDER BY surname, initials");
  $result->bind_param('si', $type, $paperID);
  $result->execute();
  $result->bind_result($id, $title, $initials, $surname);
  while ($result->fetch()) {
    $reviewer_data[$id]['title'] = $title;
    $reviewer_data[$id]['initials'] = $initials;
    $reviewer_data[$id]['surname'] = $surname;
  }
  $result->close();

  // Capture reviewer comments data first.
  $result = $mysqli->prepare("SELECT q_id, comment, category, DATE_FORMAT(reviewed,'%d/%m/%Y %T') AS reviewed, reviewer, action, response FROM review_comments WHERE review_type = ? AND q_paper = ?");
  $result->bind_param('si', $type, $paperID);
  $result->execute();
  $result->bind_result($tmp_q_id, $comment, $category, $reviewed, $tmp_reviewer, $action, $response);
  while ($result->fetch()) {
    $comments_array[$tmp_q_id][$tmp_reviewer]['reviewed'] = $reviewed;
    $comments_array[$tmp_q_id][$tmp_reviewer]['comment']  = $comment;
    $comments_array[$tmp_q_id][$tmp_reviewer]['category'] = $category;
    $comments_array[$tmp_q_id][$tmp_reviewer]['action']   = $action;
    $comments_array[$tmp_q_id][$tmp_reviewer]['response'] = $response;
  }
  $result->close();
  

  // Capture the paper makeup.
  $display_header = true;
  $question_no = 0;
  $old_q_id = 0;
  $old_screen = 1;
  $options_buffer = array();
  $correct_buffer = array();

  $result = $mysqli->prepare("SELECT paper_title, labelcolor, themecolor, screen, q_id, q_type, theme, scenario, leadin, option_text, display_method, score_method, q_media, q_media_width, q_media_height, correct, std FROM (properties, papers, questions) LEFT JOIN options ON questions.q_id = options.o_id WHERE papers.paper = properties.property_id AND papers.question = questions.q_id AND papers.paper = ? ORDER BY screen, display_pos, id_num");
  $result->bind_param('i', $paperID);
  $result->execute();
  $result->store_result();
  $result->bind_result($paper_title, $labelcolor, $themecolor, $screen, $q_id, $q_type, $theme, $scenario, $leadin, $option_text, $display_method, $score_method, $q_media, $q_media_width, $q_media_height, $correct, $std);
  while ($result->fetch()) {
    if ($display_header == true) {
      echo '<tr><th><div class="breadcrumb"><a href="../staff/index.php">' . $string['home'] . '</a>';
      if (isset($_GET['folder']) and $_GET['folder'] != '') {
        echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../folder/details.php?folder=' . $_GET['folder'] . '">' . folder_utils::get_folder_name($_GET['folder'], $mysqli) . '</a>';
      } elseif (isset($_GET['module']) and $_GET['module'] != '') {
        echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../folder/details.php?module=' . $_GET['module'] . '">' . module_utils::get_moduleid_from_id($_GET['module'], $mysqli) . '</a>';
      }
      echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../paper/details.php?paperID=' . $_GET['paperID'] . '">' . $paper . '</a></div>';

      echo "<span style=\"margin-left:10px; font-size:200%; color:black; font-weight:bold\">" . $string[$type . 'report'] . "</span></th><th style=\"text-align:right; vertical-align:top\"><img src=\"../artwork/toprightmenu.gif\" id=\"toprightmenu_icon\"></th></tr>\n";
      echo "</table>\n";
      if (count($reviewer_data) == 0) {
				echo "<table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" style=\"margin: 0px auto; width:75%; border: 1px solid #C0C0C0; text-align:left\">\n<tr><td colspan=\"2\" style=\"background-color:#F2B100; height:3px\"> </td></tr>\n<tr><td style=\"width:16px; padding-top:5px; padding-bottom:5px\"><img src=\"../artwork/information_icon.gif\" width=\"16\" height=\"16\" alt=\"i\" border=\"0\" /></td><td style=\"padding-top:5px; padding-bottom:5px\">&nbsp;" . $string['noreviewers'] . "</td></tr></table>\n<div>\n</body>\n</html>";
        exit;
      }
      echo '<br /><table cellpadding="0" cellspacing="0" border="0" width="100%">';
      $display_header = false;
    }
    if ($question_no == 0) {
      $old_labelcolor = $labelcolor;
      $old_themecolor = $themecolor;
    }
    if ($old_q_id != $q_id and $old_q_id > 0) {   // New question.
      $question_no++;
      if ($old_q_type == 'info') $question_no--;
      displayQuestion($question_no, $old_q_id, $old_theme, $old_scenario, $old_leadin, $old_q_type, $old_correct, $old_q_media, $old_q_media_width, $old_q_media_height, $options_buffer, $comments_array, $correct_buffer, $old_display_method, $old_score_method, $old_labelcolor, $old_themecolor, $old_std, $reviewer_data, $type, $string, $language);
      $options_buffer = array();
      $correct_buffer = array();
      if ($old_screen != $screen) {
        echo '<tr><td colspan="2">';
        echo '<div class="screenbrk"><span class="scr_no">' . $string['screen'] . '&nbsp;' . $screen . '</span></div>';
        echo '</td></tr>';
      }
    }
    if ($q_type == 'labelling') {
      $tmp_first_split = explode(';', $correct);
      $tmp_second_split = explode('$', $tmp_first_split[11]);
      for ($label_no = 4; $label_no <= 43; $label_no += 4) {
        if (substr($tmp_second_split[$label_no],0,1) != '|') {
          $options_buffer[] = trim(substr($tmp_second_split[$label_no],0,strpos($tmp_second_split[$label_no],'|'))) . '|' . $tmp_second_split[$label_no-2] . '|' . ($tmp_second_split[$label_no-1] - 25);
          if ($tmp_second_split[$label_no-2] > 150) {
            $correct_buffer[] = $tmp_second_split[$label_no-2] . 'x' . ($tmp_second_split[$label_no-1] - 25);
          }
        }
      }
    } elseif ($q_type == 'blank') {
      $blank_details = explode('[blank',$option_text);
      $no_answers = count($blank_details) - 1;
      for ($i=1; $i<=$no_answers; $i++) {
        $blank_details[$i] = preg_replace("| mark=\"([0-9]{1,3})\"|","",$blank_details[$i]);
        $blank_details[$i] = preg_replace("| size=\"([0-9]{1,3})\"|","",$blank_details[$i]);

        $blank_details[$i] = substr($blank_details[$i],(strpos($blank_details[$i],']') + 1));
        $blank_details[$i] = substr($blank_details[$i],0,strpos($blank_details[$i],'[/blank]'));
        $answer_list = explode(',',$blank_details[$i]);
        $answer_list[0] = str_replace("[/blank]",'',$answer_list[0]);
        if ($score_method == 'textboxes') {
          foreach ($answer_list as $individual_answer) {
            $correct_buffer[] = html_entity_decode(trim($individual_answer));
          }
        } else {
          $correct_buffer[] = html_entity_decode(trim($answer_list[0]));
        }
      }
      $options_buffer[] = $option_text;
    } else {
      $options_buffer[] = $option_text;
      $correct_buffer[] = $correct;
    }
    $old_q_id = $q_id;
    $old_theme = $theme;
    $old_scenario = $scenario;
    $old_leadin = $leadin;
    $old_q_type = $q_type;
    $old_q_media = $q_media;
    $old_q_media_width = $q_media_width;
    $old_q_media_height = $q_media_height;
    $old_correct = $correct;
    $old_display_method = $display_method;
    $old_score_method = $score_method;
    $old_std = $std;
    $old_screen = $screen;
  }
  $result->close();
  $question_no++;
  if ($old_q_type == 'info') $question_no--;
  displayQuestion($question_no, $old_q_id, $old_theme, $old_scenario, $old_leadin, $old_q_type, $old_correct, $old_q_media, $old_q_media_width, $old_q_media_height, $options_buffer, $comments_array, $correct_buffer, $old_display_method, $old_score_method, $old_labelcolor, $old_themecolor, $old_std, $reviewer_data, $type, $string, $language);
  $mysqli->close();
?>
</table>
<input type="hidden" name="scrOfY" id="scrOfY" value="0" />
</form>
</div>
</body>
</html>
