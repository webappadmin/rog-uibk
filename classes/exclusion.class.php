<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Class to handle question exclusions on papers.
 *
 * @author Simon Wilkinson
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

class Exclusion {

  private $db;
  private $paper_id;   

	/**
	 * @param int $paperID  - ID of the current paper.
	 * @param object $db    - Link to mysqli
	 */
	 public function __construct($paperID, $db) {
  	$this->db = $db;
    $this->paper_id = $paperID;  		
  }

  public function load() {
    $this->excluded = array();
    $result = $this->db->prepare("SELECT q_id, parts FROM question_exclude WHERE q_paper = ? ORDER BY q_id");
    $result->bind_param('i', $this->paper_id);
    $result->execute();
    $result->bind_result($q_id, $parts);
    while ($result->fetch()) {
      $this->excluded[$q_id] = $parts;
    }
    $result->close();
  }
  
  public function clear_all_exclusions() {
    // Clear the database of any past exclusions from the current paper.
    if ($result = $this->db->prepare("DELETE FROM question_exclude WHERE q_paper = ?")) {
      $result->bind_param('i', $this->paper_id);
      $result->execute();
      $result->close();
    } else {
      display_error("Question_exclude Delete Error", $this->db->error);
    }
  }
  
  public function add_exclusion($q_id, $status) {
    $userObj = UserObject::get_instance();

    if ($result = $this->db->prepare("INSERT INTO question_exclude VALUES (NULL, ?, ?, ?, {$userObj->get_user_ID()}, NOW(), '')")) {
      $result->bind_param('iis', $this->paper_id, $q_id, $status);
      $result->execute();
      $result->close();
    } else {
      display_error("Question_exclude Insert Error 1", $this->db->error);
    }
  }
  
  public function get_exclusions_by_qid($q_id) {
    if (!isset($this->excluded[$q_id])) {
      return '0000000000000000000000000000000000000000';
    } else {
      return $this->excluded[$q_id];
    }
  }
  
  public function is_question_excluded($q_id) {
    if (isset($this->excluded[$q_id]) and strpos($this->excluded[$q_id], '1') !== false) {
      return true;
    } else {
      return false;
    }
  }
  
  public function get_excluded_no() {
    return count($this->excluded);
  }

}