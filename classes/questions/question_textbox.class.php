<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Class for Multiple Choice questions
 *
 * @author Rob Ingram
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

Class QuestionTEXTBOX extends QuestionEdit {

  protected $columns = 80;
  protected $rows = 4;
  public $max_options = 1;
  protected $_allow_change_marking_method = false;

  protected $_fields_editable = array('theme', 'scenario', 'leadin', 'notes', 'correct_fback', 'incorrect_fback', 'rows', 'columns', 'bloom', 'status');
  protected $_fields_settings = array('columns', 'rows');

  function __construct($mysqli, $userObj, $lang_strings, $data = null) {
    parent::__construct($mysqli, $userObj, $lang_strings, $data);

    $this->_fields_unified = array('correct' => $this->_lang_strings['terms'], 'text' => $this->_lang_strings['editor'], 'marks_correct' => $this->_lang_strings['markscorrect'], 'marks_incorrect' => $this->_lang_strings['marksincorrect']);
  }


  // ACCESSORS

  /**
   * Get the columns for the question
   * @return integer
   */
  public function get_columns() {
    return $this->columns;
  }

  /**
   * Set the columns for the question
   * @param integer $value
   */
  public function set_columns($value) {
    if ($value != $this->columns) {
      $this->set_modified_field('columns', $this->columns);
      $this->columns = $value;
    }
  }

  /**
   * Get the rows for the question
   * @return integer
   */
  public function get_rows() {
    return $this->rows;
  }

  /**
   * Set the rows for the question
   * @param integer $value
   */
  public function set_rows($value) {
    if ($value != $this->rows) {
      $this->set_modified_field('rows', $this->rows);
      $this->rows = $value;
    }
  }
}

