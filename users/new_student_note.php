<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* Add a note to a students file
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/staff_auth.inc';
require_once '../classes/userutils.class.php';
require_once '../classes/dateutils.class.php';
require_once '../classes/noteutils.class.php';

if (isset($_POST['submit'])) {
	if ($_POST['note_id'] == '' or $_POST['note_id'] == '0') {
		StudentNotes::add_note($_POST['userID'], $_POST['note'], $_POST['paperID'], $userObject->get_user_ID(), $mysqli);
	} else {
		StudentNotes::update_note($_POST['note'], $_POST['note_id'], $mysqli);
	}
?>
<!DOCTYPE html>
  <html>
  <head><title><?php echo $string['note']; ?></title>
  <?php
    if ($_POST['calling'] == 'class_totals') {
  ?>
  <script language="JavaScript">
    function closeWindow() {
      window.opener.location.reload();
      window.close();
    }
  </script></head>
  <body onload="window.opener.location.reload(); closeWindow();">
  <?php
    } else {
  ?>
  <script language="JavaScript">
    function closeWindow() {
      window.opener.location = "details.php?userID=<?php echo $_POST['userID']; ?>&tab=notes";
      window.close();
    }
  </script></head>
  <body onload="closeWindow();">
  <?php
    }
  ?>
  <form>
    <br />&nbsp;<div align="center"><input type="button" name="home" value="   OK   " onclick="closeWindow();" /></div>
  </form>
  <?php
  } else {
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  
  <title><?php echo $string['note']; ?></title>

  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/notes.css" />
  
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/jquery.validate.min.js"></script>
  <script language="JavaScript">
    $(document).ready(function() {
	    var noteHeight = $(document).height() - 140;
	    $("#note").css('height', noteHeight + 'px')
      $("#note").focus();
			
      $('#theform').validate({
        errorClass: 'errfield',
        errorPlacement: function(error,element) {
          return true;
        }
      });
      $('form').removeAttr('novalidate');
    });
	 
	  $(window).resize(function() {
	    var noteHeight = $(document).height() - 140;
	    $("#note").css('height', noteHeight + 'px')
	  });
   
    $('#theform').submit(function() {
      if ($("#paperID").val() == '') {
        alert("<?php echo $string['namecheck']; ?>");
        return false;
      }
   
      if ($("#note").val() == '') {
        alert("<?php echo $string['notecheck']; ?>");
        return false;
      }
     
      return true;
    });
  </script>
</head>

<body>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" name="theform" id="theform">
<?php
	$note_details = array('note_id'=>0, 'note'=>'');
	
	$student_details = UserUtils::get_user_details($_GET['userID'], $mysqli);
  
	if (isset($_GET['paperID'])) {
    echo "<input type=\"hidden\" name=\"paperID\" value=\"" . $_GET['paperID'] . "\" />\n";
		
		$note_details = StudentNotes::get_note($_GET['paperID'], $_GET['userID'], $mysqli);
		
    echo '<strong>' . $student_details['title'] . ' ' . $student_details['surname'] . ', ' . $student_details['initials'] . '</strong><br />';
  } else {
		$student_modules = UserUtils::load_student_modules($_GET['userID'], $mysqli);
		
		$current_year = date_utils::get_current_academic_year();
		$module_IDs = array();
		foreach ($student_modules[$current_year] as $moduleID=>$module_code) {
			$module_IDs[] = $moduleID;
		}
		
    echo $string['papername'] . " <select name=\"paperID\" id=\"paperID\" required>\n<option value=\"\"></option>\n";
    $result = $mysqli->prepare("SELECT DISTINCT properties.property_id, paper_title FROM properties, properties_modules WHERE properties.property_id = properties_modules.property_id AND idMod IN (" . implode(',', $module_IDs) . ") AND paper_type = '2' AND end_date > DATE_SUB(NOW(), INTERVAL 28 DAY) AND deleted IS NULL ORDER BY paper_title");
		$result->execute();
    $result->bind_result($property_id, $paper_title);
    while ($result->fetch()) {
      echo "<option value=\"$property_id\">$paper_title</option>\n";
    }
    echo "</select>\n<br />\n";
    $result->close();
  }
  
  echo "<br />" . $string['note'] . "<br />\n";
  echo "<div style=\"text-align:center\"><textarea name=\"note\" id=\"note\" required>" . $note_details['note'] . "</textarea></div>\n";
?>
<br />
<div style="text-align:center"><input type="submit" style="width:100px" name="submit" value="<?php echo $string['save']; ?>" />&nbsp;<input style="width:100px" type="button" name="cancel" value="<?php echo $string['cancel']; ?>" onclick="javascript:window.close();" /></div>
<input type="hidden" name="userID" value="<?php echo $_GET['userID']; ?>" />
<input type="hidden" name="calling" value="<?php if (isset($_GET['calling'])) echo $_GET['calling']; ?>" />
<input type="hidden" name="note_id" value="<?php echo $note_details['note_id']; ?>" />
</form>

</body>
</html>
<?php
}
$mysqli->close();
?>