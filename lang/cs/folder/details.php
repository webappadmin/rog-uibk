<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/paper_options.inc';
require_once '../lang/' . $language . '/include/paper_types.inc';

$string['showretired'] = 'Zobrazit neplatné';
$string['teammembers'] = 'Členové týmu';
$string['calendar'] = 'Kalendář';
$string['edit'] = 'Upravit';
$string['screen'] = 'Obrazovka';
$string['screens'] = 'Obrazovky';
$string['mins'] = 'minut';
$string['SelfenrolURL'] = 'URL pro samo-přihlášení';
$string['createnewpaper'] = 'Nový dokument';
$string['editpropertiessysadmin'] = 'Upravit vlastnosti (SysAdmin)';
$string['manageobjectives'] = 'Správa cílů';
$string['managekeywords'] = 'Správa klíčových slov';
$string['referencematerial'] = 'Referenční materiál';
$string['importstudentmetadata'] = 'Nahrát metadata studenta';
$string['listpapers'] = 'Přehled dokumentů';
$string['module'] = 'Modul';
$string['papertasks'] = 'Papírové testy';
$string['questionbank'] = 'Banka úloh';
$string['foldertasks'] = 'Úlohy ke složce';
$string['folderproperties'] = 'Vlastnosti složky';
$string['makesubfolder'] = 'Vytvořit podsložku';
$string['deletefolder'] = 'Odstranit složku';
$string['questionsbytype'] = 'Úlohy podle typu';
$string['questionsbyteam'] = 'Úlohy podle týmů';
$string['questionsbykeyword'] = 'Úlohy podle klíčových slov';
$string['search'] = 'Hledat';
$string['createnewquestion'] = 'Nová úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Lidé';
$string['type'] = 'Typ';
$string['author'] = 'Autor';
$string['create'] = 'Vytvořit';
$string['newfolder'] = 'Nová složka';
$string['sudentson'] = 'Studenti skládající zkoušku ';
$string['nomodulesset'] = 'Není nastaven žádný modul';
?>