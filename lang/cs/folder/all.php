<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['allmodules'] = 'Všechny moduly';
$string['bymodulecode'] = 'Podle kódu modulu';
$string['admintools'] = 'Nastavení';
$string['calendar'] = 'Kalendář';
$string['usermanagement'] = 'Nastavení uživatelů';
$string['makeafolder'] = 'Vytvořit složku';
$string['mypersonalkeywords'] = 'Moje klíčová slova';
$string['papertasks'] = 'Papírové testy';
$string['createnewpaper'] = 'Nový dokument';
$string['listpapers'] = 'Přehled dokumentů';
$string['mymodules'] = 'Moje moduly';
$string['sysadminonly'] = 'Pouze systémový správce';
$string['adminonly'] = 'Pouze správce';
$string['create'] = 'Vytvořit';
$string['newfolder'] = 'Nová složka';
$string['questionbanktasks'] = 'Správa banky úloh';
$string['questionsbytype'] = 'Úlohy podle typu';
$string['questionsbyteam'] = 'Úlohy podle týmů';
$string['questionsbykeyword'] = 'Úlohy podle klíčových slov';
$string['search'] = 'Hledat';
$string['createnewquestion'] = 'Nová úloha';
$string['questions'] = 'Úlohy';
$string['papers'] = 'Dokumenty';
$string['people'] = 'Lidé';
?>