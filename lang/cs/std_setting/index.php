﻿<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['listsettings'] = 'Seznam nastavení';
$string['standardssetting'] = 'Standardní nastavení';
$string['standardsetter'] = 'Nastavení standardů';
$string['date'] = 'Datum';
$string['passscore'] = 'Potřebné skóre';
$string['distinction'] = 'S vyznamenáním';
$string['reviewmarks'] = 'Přehled známek';
$string['papertotal'] = 'Přehled dokumentu';
$string['method'] = 'Metoda';
$string['aboutstandardssetting'] = 'O standardním nastavení';
$string['createmodifiedangoff'] = 'Nová modifikovaná Angoffova metoda';
$string['createebelmethod'] = 'Nová Ebelova metoda';
$string['editrating'] = 'Upravit ohodnocení';
$string['delete'] = 'Odstranit';
$string['modifiedangoff'] = 'Modifikovaná Angoffova metoda';
$string['individualrating'] = 'Individuální ohodnocení';
$string['currentrating'] = 'Aktuální ohodnocení';
$string['grouprating'] = 'Ohodnocení skupiny';
$string['createhofsteemethod'] = 'Nová Hofstedeova metoda';
?>