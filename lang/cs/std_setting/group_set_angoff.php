<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/std_setting/std_set_shared.php';

$string['angoffmethod'] = 'Angoffova metoda';
$string['groupreview'] = 'Skupinové hodnocení';
$string['percentagemsg'] = 'Pomocí bledě-modrého rozbalovacího seznamu, vedle každé úlohy, označte <strong>minimum</strong> studentů, jež očekáváte, že odpoví správně.';
$string['warningmsg'] = ' = Recenze se liší o více než 10%';
$string['changedmsg'] = '<strong>Varování</strong>: Jedno, či více individuálních ohodnocení, ze kterých je utvořeno ohodnocení skupiny, se změnilo/a od doby, kdy se ohodnocení tvořilo. Toto/tyto ohodnocení nejsou zahrnuta/y v průměrných hodnotách vyobrazených níže.';
$string['note'] = 'Poznámka';
$string['updatepassmark'] = 'Aktualizovat Potřebnou známku';
$string['saveratings'] = 'Uložit ohodnocení';
$string['screen'] = 'Obrazovka';
?>