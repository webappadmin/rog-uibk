<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/user_search_options.inc';

$string['sendwelcomeemail'] = 'Send welcome email to user';
$string['importstudents'] = 'Nahrát Studenty';
$string['csvfile'] = 'CSV File:';
$string['import'] = 'Importovat';
$string['msg1'] = 'Rogō can bulk upload student details and create new accounts from CSV files. The first row should be a header row containing the following fields:';
$string['msg2'] = "Další (nepovinná) pole 'Moduly' a 'Relace' mohou být přidána a použita k hromadnému zápisu nových studentů do daných modulů.";
$string['loading'] = 'Loading...';
$string['followingerrors'] = 'No users added due to the following errors:';
$string['usersadded'] = 'users added';
$string['usersupdated'] = 'existing users updated';
$string['missingcolumn'] = 'Missing \'%s\' Colum from import please add it.';
$string['finished'] = 'Finished';
$string['loadstudents'] = 'Rogō: Load Students';
$string['emailmsg1'] = 'Vytvořit nový uživatelský účet';
$string['emailmsg2'] = 'Milá/ý';
$string['emailmsg3'] = 'Byl vytvořen nový uživatelský účet, pomocí kterého se můžete přihlásit do systému on-line elektronického testování Rogō. Vaše osobní ověřovací údaje jsou totožné s univerzitním přihlašovacím jménem.';
$string['emailmsg4'] = 'Poznámka:';
$string['emailmsg5'] = 'Nikdy se s nikým o Vaše univerzitní uživatelské jméno a heslo nedělte.';
$string['emailmsg6'] = 'Opisování při sumativních zkouškách je akademickým přestupkem a nebude tolerováno.';
$string['emailmsg7'] = 'Odeslání e-mailu selhalo.';
?>