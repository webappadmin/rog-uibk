<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Přístup dohlížejícího';
$string['lab'] = 'Učebna:';
$string['unknownlab'] = ' - neznámá učebna';
$string['nopapersfound'] = 'Nebyly nalezeny žádné dokumenty!';
$string['nopapersfoundmsg'] = 'K této učebně nelze, v doučasnosti, dohledat žádné dokumenty.';
$string['emergencynumbers'] = 'Čísla na pohotovost';
$string['name'] = 'Jméno';
$string['extension_mins'] = 'Prodloužení (minuty)';
$string['endtime'] = 'Čas ukončení';
$string['extendtime'] = 'Prodloužení';
$string['extendtimeby'] = 'Prodloužení času o';
$string['addnote']        = 'Přidat poznámku';
$string['currenttime']    = 'Aktuální čas:';
$string['start'] = 'Start:';
$string['end']       = 'Konec';
$string['start_but'] = 'Start'; 
$string['endat_but']  = 'Konec v';
$string['session_end'] = 'Konec relace';
$string['duration'] = 'Trvání:';
$string['mins'] = 'minut';
$string['papernote'] = 'Poznámky dokumentu';
$string['extratime'] = 'Prodloužení';
$string['checklist'] = '<div><strong>Seznam úkonů</strong></div>
    <div><em>Před zkouškou</em></div>
    <ol>
    <li>Umístěte přihlašovací pokyny ke každému počítači, který bude obsazen. </li>
    <li>Ke každému počítači, který bude obsazen, položte prázdný papír. </li>
    <li>Zkontrolujte, zda jsou všichni studenti správně přihlášeni.</li>
    <li>Každému, kdo není schopen se přihlásit, doporučte využít \'Dočasný\' účet.  </li>
    <li><strong>Poznámka:</strong> Nezačínejte před plánovaným časem zahájení!</li>
    </ol>
    
    <div><em>V průběhu zkoušky</em></div>
    <ol>
    <li>Zaznamenejte drobné problémy studentů\' do souboru (<a href="%shelp.html" target="_blank">příklady problémů</a>) . </li>
    <li>Zaznamenejte problémy s obsahem dokumentů/úloh. </li>
    <li>Při větších problémech volejte na níže uvedené číslo. </li>
    </ol>
    
    <div><em>Po zkoušce</em></div>
    <ol start="4">
    <li>"Tímto zkouška končí. Prosím, přejděte na poslední okno a klikněte na tlačítko \'Konec\'."</li>
    <li>"Klikněte na \'Zavřít okno\', poté stiskněte CTRL, ALT, DELETE; tímto se odhlásíte z pracovního počítače."</li>
    <li>Posbírejte všechny papíry s přihlašovacími pokyny k opětovnému použití.</li>
    <li>Prázdné papíry posbírejte a odložte.</li>
    <li>Ujistěte se, že <strong>všechny</strong> využité počítače jsou odhlášené. </li>
    </ol>';
$string['time'] = 'Čas';
$string['timedexam'] = 'Zkouška s časovým limitem:';
$string['timeerror'] = 'Chyba v čase';
$string['timeerrormsg'] = 'Zkouška musí trvat minimálně %d minut.';
$string['examquestionclarifications'] = 'Exam question clarifications';
?>