<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['questionstatuses'] = 'Status Otázky';
$string['createstatus'] = 'Nový Status';
$string['edit'] = 'Upravit';
$string['add'] = 'Přidat';
$string['status'] = 'Status'; //cognate
$string['deletestatus'] = 'Smazat Status';
$string['administrativetools'] = 'Nastavení';
$string['statuses'] = 'Statusy';
$string['name'] = 'Název';
$string['excludemarking'] = 'Neznámkovat';
$string['retired'] = 'Neplatný';
$string['default'] = 'Standardní';
$string['setlocked'] = 'Lze nastavit u zamknutých otázek';
$string['validate'] = 'Ověřit';
$string['displaywarning'] = 'Zobrazit upozornění';
$string['colour'] = 'Barva';
$string['enternameofstatus'] = 'Zadejte, prosím Název statusu.';
$string['duplicateerror'] = 'Názvy statusů se nesmí shodovat.';
?>