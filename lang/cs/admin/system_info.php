<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['systeminformation'] = 'Systémové informace';
$string['administrativetools'] = 'Nastavení';
$string['table'] = 'Tabulka';
$string['records'] = 'Záznamy';
$string['updated'] = 'Aktualizováno';
$string['engine'] = 'Stroj';
$string['application'] = 'Rogō aplikace';
$string['rogoplugins'] = 'Rogō plug-ins';
$string['version'] = 'Verze';
$string['webroot'] = 'Web Root'; //cognate
$string['database'] = 'Databáze';
$string['authentication'] = 'Autentizace';
$string['serverinformation'] = 'Informace o serveru';
$string['processor'] = 'Procesor';
$string['cpus'] = 'CPUs'; //cognate
$string['cores'] = 'Jádra';
$string['servername'] = 'Název serveru';
$string['hostname'] = 'Název Hostu'; 
$string['ipaddress'] = 'IP Addresa';
$string['clock'] = 'Čas';
$string['os'] = 'OS'; //cognate
$string['apache'] = 'Apache'; //cognate
$string['php'] = 'PHP'; //cognate
$string['mysql'] = 'MySQL'; //cognate
$string['clientcomputer'] = 'Klientský počítač';
$string['browser'] = 'Prohlížeč';
$string['partitions'] = 'Rozdělení';
$string['mysqlstatus'] = 'MySQL Status'; //cognate
$string['uptime'] = 'Nahozeno již';
$string['threads'] = 'Vlákna';
$string['questions'] = 'Úlohy';
$string['slow queries'] = 'Slow queries'; 
$string['opens'] = 'Opens'; 
$string['flush tables'] = 'Flush tables'; 
$string['open tables'] = 'Open tables'; 
$string['queries per second avg'] = 'Queries per second avg'; 
$string['na'] = 'N/A'; //cognate
$string['driveicon'] = 'Drive icon'; 
$string['freespace'] = '%s volných z %s';
$string['More details'] = 'Další detaily...';
$string['lookups'] = 'Lookups'; 
$string['interactivequestions'] = 'Interactive Questions';
$string['minutes'] = 'minuty';
$string['hours'] = 'hodiny';
$string['days'] = 'dny';
$string['Session'] = 'Relace';
$string['EnhancedCalcPlugin'] = 'Calculation question plugin';
$string['ErrorLogSettings'] = 'Error Reporting';
$string['authdebug'] = 'Show authentication debug';
$string['errorsonscreen'] = 'Show errors on-screen';
$string['phpnotices'] = 'Show PHP Notices';
$string['errorshutdown'] = 'Error handling at shutdown';
$string['varcapturemethod'] = 'Variable capture method on error:';
$string['improved'] = 'Improved';
$string['basic'] = 'Basic';
$string['none'] = 'none';
$string['company'] = 'Company';
?>