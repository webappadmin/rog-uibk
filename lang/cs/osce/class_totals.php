<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['classtotals'] = 'Celkový výkaz třídy';
$string['classtotalsmodule'] = 'Celkový výkaz třídy (%s pouze studenti)';
$string['name'] = 'Jméno';
$string['studentid'] = 'ID studenta';
$string['course'] = 'Kurz';
$string['total'] = 'Celkem';
$string['classification'] = 'Klasifikace';
$string['starttime'] = 'Čas/Datum zahájení';
$string['examiner'] = 'Zkoušející';
$string['summary'] = 'Přehled';
$string['cohortsize'] = 'Velikost skupiny';
$string['fail'] = 'Neuspěl:';
$string['pass'] = 'Uspěl:';
$string['error'] = 'CHYBA';
$string['noattendance'] = 'Nepřítomni';
$string['clear fail'] = 'Neuspěl';
$string['borderline'] = 'Na hraně';
$string['clear pass'] = 'Uspěl';
$string['borderline fail'] = 'Dostatečně';
$string['borderline pass'] = 'Dobře';
$string['good pass'] = 'Chvalitebně';
$string['honours pass'] = 'Uspěl s vyznamenáním';
$string['oscemarksheet'] = 'OSCE známkovací formulář';
$string['feedback'] = 'Komentář';
$string['studentprofile'] = 'Profil studenta';
$string['unsatisfactory'] = 'Nevyhovující:';
$string['competent'] = 'Schopný:';
$string['passmark'] = 'Potřebná známka';
$string['title'] = 'Nadpis';
$string['surname'] = 'Příjmení';
$string['firstnames'] = 'Jméno';
$string['rating'] = 'Ohodnocení';
$string['borderlinemethod'] = 'Hraniční metoda';
$string['noattempts'] = 'This OSCE has not been attempted by any students between <strong>%s &ndash; %s</strong>.';
?>