<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['noadd'] = 'Žádné přidané';
$string['activepaper'] = 'Varování: aktivní dokument';
$string['msg'] = 'Do tohoto dokumentu nelze přidat další úlohy, jelikož je v současnosti aktivní.<br /><br />To je bezpečnostní funkce, zabrání úpravě dokumentu během možného průběhu zkoušky.';
$string['solution'] = 'Řešení:';
$string['solutionmsg'] = 'Otevřete <a href="#" onclick="paperProperties(); return false;"><img src="../../artwork/small_link.png" width="12" height="12" alt="Shortcut" border="0" /></a> <a href="#" style="color:blue" onclick="paperProperties(); return false;">Upravit vlastnosti</a> v podokně \'Úkoly aktuálního dokumentu\' a změnit dostupné termíny.';
$string['to'] = 'do';
?>