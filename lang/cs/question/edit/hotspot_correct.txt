<?php         //cz

$string['hotspots'] = 'Úlohy s aktivním místem v obrázku';
$string['correctAnswers'] = 'Správné odpovědi';
$string['incorrectAnswers'] = 'Chybné odpovědi';
$string['errorcanvas'] = 'Canvas not supported';
$string['errorimageshotspot'] = 'Hotspot question cannot be displayed because some images were not loaded.';

?>