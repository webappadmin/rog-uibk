<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_security.inc';

$string['peerreview'] = 'Recenze';
$string['Reviewer'] = 'Recenzent';
$string['save'] = 'Uložit';
$string['close'] = 'Zavřít';
$string['Review Form'] = 'Formulář recenzenta';
$string['Student Profile'] = 'Profil studenta';
$string['Group'] = 'Skupina';
$string['Thank You'] = 'Děkujeme Vám';
$string['The ratings saved'] = 'Ohodnocení uloženo.';

?>