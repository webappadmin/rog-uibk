<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/paper_options.inc';
require_once '../lang/' . $language . '/include/paper_types.inc';

$string['showretired'] = 'Show retired';
$string['teammembers'] = 'Team Members';
$string['calendar'] = 'Calendar';
$string['edit'] = 'Edit';
$string['screen'] = 'Screen';
$string['screens'] = 'Screens';
$string['mins'] = 'mins';
$string['SelfenrolURL'] = "Self-enrol URL";
$string['createnewpaper'] = 'Create new Paper';
$string['editpropertiessysadmin'] = 'Edit Properties (SysAdmin)';
$string['manageobjectives'] = 'Manage Objectives';
$string['managekeywords'] = 'Manage Keywords';
$string['referencematerial'] = 'Reference Material';
$string['importstudentmetadata'] = 'Import Student metadata';
$string['listpapers'] = 'List Papers';
$string['module'] = 'Module';
$string['papertasks'] = 'Paper Tasks';
$string['questionbank'] = 'Question Bank';
$string['foldertasks'] = 'Folder Tasks';
$string['folderproperties'] = 'Folder Properties';
$string['makesubfolder'] = 'Make a new sub-folder';
$string['deletefolder'] = 'Delete this folder';
$string['questionsbytype'] = 'Questions by Type';
$string['questionsbyteam'] = 'Questions by Team';
$string['questionsbykeyword'] = 'Questions by Keyword';
$string['search'] = 'Search';
$string['createnewquestion'] = 'Create new Question';
$string['questions'] = 'Questions';
$string['papers'] = 'Papers';
$string['people'] = 'People';
$string['type'] = 'Type';
$string['author'] = 'Author';
$string['create'] = 'Create';
$string['newfolder'] = 'New Folder';
$string['sudentson'] = 'Students on ';
$string['nomodulesset'] = 'No modules set';
?>