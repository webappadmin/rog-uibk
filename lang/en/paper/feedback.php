<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/paper/finish.php';

$string['nofeedback'] = 'This paper is not set to display feedback currently.';
$string['nottaken'] = 'Feedback not available, you have not taken this paper.';
$string['norights'] = 'You do not have rights to see this paper.';
$string['msg'] = '<strong>University Rules</strong><br />1) no leaving in first hour, 2) no leaving in last 15 minutes.<br /><br />If you comply with the first two rules and there is only one sitting of your exam then you may click \'Close Window\' and then press &lt;CTRL&gt; &lt;ALT&gt; and &lt;DELETE&gt; to log out of your workstation.';
?>