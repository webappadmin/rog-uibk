<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Invigilator Access';
$string['lab'] = 'Lab:';
$string['unknownlab'] = ' - unknown lab';
$string['nopapersfound'] = 'No papers found!';
$string['nopapersfoundmsg'] = 'No assessments can be found for the current lab at this time.';
$string['emergencynumbers'] = 'Emergency Numbers';
$string['name'] = 'Name';
$string['extension_mins'] = 'Extra&nbsp;(mins)';
$string['endtime'] = 'End';
$string['extendtime'] = 'Extend Time';
$string['extendtimeby'] = 'Extend Time By';
$string['addnote']        = 'Add note';
$string['currenttime']    = 'Current Time:';
$string['start']     = 'Start';
$string['end']       = 'End';
$string['start_but'] = 'Start'; 
$string['endat_but']  = 'End At';
$string['session_end'] = 'Session End';
$string['duration']  = 'Duration';
$string['mins']      = 'mins';
$string['papernote'] = 'Paper Note';
$string['extratime'] = 'Extra Time';
$string['checklist'] = '<div><strong>Tasklist</strong></div>
    <div><em>Pre-Exam</em></div>
    <ol>
    <li>Place log in instructions at each workstation</li>
    <li>Place blank paper each workstation</li>
    <li>Check all students have logged in correctly</li>
    <li>Use \'guest\' accounts for anyone not able to log in</li>
    <li><strong>NOTE:</strong> Do not start before scheduled start time</li>
    </ol>

    <div><em>Mid-Exam</em></div>
    <ol>
    <li>Record minor problems in the students\' file (<a href="%shelp.html" target="_blank">example problems</a>)</li>
    <li>Record problems with paper/question content</li>
    <li>Call number below for major problems</li>
    </ol>

    <div><em>Post-Exam</em></div>
    <ol start="4">
    <li>"That is the end of the exam. Please navigate to the last screen and click \'Finish\'."</li>
    <li>"Click \'Close Window\' and then CTRL, ALT and DELETE and log out of your workstation."</li>
    <li>Collect up log in instructions for reuse</li>
    <li>Collect and dispose of blank paper</li>
    <li>Ensure <strong>all</strong> workstations are logged out</li>
    </ol>';
$string['time'] = 'Time';
$string['timedexam'] = 'Timed Exam:';
$string['timeerror'] = 'Time Error';
$string['timeerrormsg'] = 'Exam must be at least %d minutes in length';
$string['examquestionclarifications'] = 'Exam question clarifications';
?>