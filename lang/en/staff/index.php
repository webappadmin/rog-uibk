<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['admintools'] = 'Administrative Tools';
$string['calendar'] = 'Calendar';
$string['usermanagement'] = 'User Management';
$string['makeafolder'] = 'Make a new folder';
$string['mypersonalkeywords'] = 'My Personal Keywords';
$string['papertasks'] = 'Paper Tasks';
$string['createnewpaper'] = 'Create new Paper';
$string['listpapers'] = 'List Papers';
$string['reviewed'] = 'Reviewed';
$string['notreviewed'] = 'Not Reviewed';
$string['myfolders'] = 'My Folders';
$string['mymodules'] = 'My Modules';
$string['sysadminonly'] = 'SysAdmin only';
$string['adminonly'] = 'Admin only';
$string['unassignedpapers'] = 'Unassigned Papers';
$string['recyclebin'] = 'Recycle Bin';
$string['allmodules'] = 'All Modules...';
$string['allmodulesinschool'] = 'All Modules in School...';
$string['myrecentpapers'] = 'My Recent Papers';
$string['create'] = 'Create';
$string['newfolder'] = 'New Folder';
$string['questionbanktasks'] = 'Question Bank Tasks';
$string['questionsbytype'] = 'Questions by Type';
$string['questionsbyteam'] = 'Questions by Module';
$string['questionsbykeyword'] = 'Questions by Keyword';
$string['search'] = 'Search';
$string['createnewquestion'] = 'Create new Question';
$string['questions'] = 'Questions';
$string['papers'] = 'Papers';
$string['people'] = 'People';
$string['nomodulesset'] = 'No modules set';
$string['screen'] = 'Screen';
$string['screens'] = 'Screens';
$string['mins'] = 'mins';
$string['type'] = 'Type';
$string['author'] = 'Author';
$string['duplicatefoldername'] = 'Duplicate folder name, please use an alternative.';
$string['loggedinas'] = 'You are logged in as';
$string['deadline'] = 'Deadline:';
$string['nomodules'] = 'You are not a member of any teams. For help please contact:';
$string['papersforreview'] = 'Papers for Review';
?>