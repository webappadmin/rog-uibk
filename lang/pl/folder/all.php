<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require '../lang/' . $language . '/include/question_types.inc';
require '../lang/' . $language . '/include/paper_types.inc';

$string['allmodules'] = 'Wszystkie moduły';
$string['bymodulecode'] = 'wg. kodu modułu';
$string['admintools'] = 'Narzędzia administracyjne';
$string['calendar'] = 'Kalendarz';
$string['usermanagement'] = 'Zarządzanie użytkownikami';
$string['makeafolder'] = 'Utwórz nowy folder';
$string['mypersonalkeywords'] = 'Moje własne słowa kluczowe';
$string['papertasks'] = 'Działania dot. arkuszy';
$string['createnewpaper'] = 'Utwórz nowy arkusz'; 
$string['listpapers'] = 'Wyświetl listę arkuszy';
$string['mymodules'] = 'Moje moduły';
$string['sysadminonly'] = 'Tylko dla superadministratora';
$string['adminonly'] = 'Tylko dla administratora';
$string['create'] = 'Utwórz';
$string['newfolder'] = 'Nowy folder';
$string['questionbanktasks'] = 'Działania dot. banku pytań';
$string['questionsbytype'] = 'Pytania wg. typu';
$string['questionsbyteam'] = 'Pytania wg. zespołu';
$string['questionsbykeyword'] = 'Pytania wg. słowa klucz.';
$string['search'] = 'Szukaj';
$string['createnewquestion'] = 'Utwórz nowe pytanie';
$string['questions'] = 'Pytania';
$string['papers'] = 'Arkusze';
$string['people'] = 'Osoby';
?>