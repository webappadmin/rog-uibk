<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

require_once '../lang/' . $language . '/include/status.inc';

$string['question'] = 'Pytanie';
$string['questionbank'] = 'Bank pytań';
$string['notinteam'] = 'Uwaga: nie jesteś w zespole';
$string['type'] = 'Typ';
$string['owner'] = 'Właściciel';
$string['modified'] = 'Zmodyfikowano';
$string['status'] = 'Status'; //cognate //Status, Stan
$string['myquestionsonly'] = 'tylko moje pytania';
$string['questionbanktasks'] = 'Działania dot. banku pytań';
$string['currentquestiontasks'] = 'Działania dot. aktualnego pytania';
$string['questionsbytype'] = 'Pytania wg. typu';
$string['questionsbyteam'] = 'Pytania wg. zespołu';
$string['questionsbykeyword'] = 'Pytania wg. słowa klucz.';
$string['search'] = 'Szukaj';
$string['createnewquestion'] = 'Utwórz nowe pytanie';
$string['questions'] = 'Pytania';
$string['papers'] = 'Arkusze';
$string['people'] = 'Osoby';
$string['quickview'] = 'Szybki podgląd';
$string['editquestion'] = 'Edytuj pytanie';
$string['information'] = 'Informacje';
$string['copyontopaperx'] = 'Przekopiuj do arkusza X...';
$string['linktopaperx'] = 'Powiąż z arkuszem X...';
$string['deletequestion'] = 'Usuń pytanie';
$string['noquestionleadin'] = 'Uwaga: brak sformułowania pytania!';
?>