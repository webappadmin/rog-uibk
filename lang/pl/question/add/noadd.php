<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['noadd'] = 'Nie dodane';  
$string['activepaper'] = 'Uwaga: Arkusz jest aktywny';
$string['msg'] = 'Nowe pytania nie mogą być dodane do tego arkusza gdy jest on aktywny.<br /><br />Jest to zabezpieczenie przez przypadkową modyfikacją arkuszy w czasie gdy egzaminowani mogliby je właśnie używać.';
$string['solution'] = 'Rozwiązanie:';
$string['solutionmsg'] = 'Wybierz opcję <a href="#" onclick="paperProperties(); return false;"><img src="../../artwork/small_link.png" width="11" height="11" alt="Shortcut" /></a> <a href="#" style="color:blue" onclick="paperProperties(); return false;">Edytuj właściwości</a> z panelu \'Działania dot. arkuszy\' i zmień daty dostępności.';
$string['to'] = 'do';
?>