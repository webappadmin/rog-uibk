<?php
// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.

$string['invigilatoraccess'] = 'Dostęp osoby nadzorującej';
$string['lab'] = 'Pracownia:';
$string['unknownlab'] = ' - nieznana pracownia';
$string['nopapersfound'] = 'Nie znaleziono arkuszy!';
$string['nopapersfoundmsg'] = 'Dla danej pracowni w tym roku nie znaleziono żadnych egzaminów.';
$string['emergencynumbers'] = 'Numery awaryjne';
$string['name'] = 'Nazwa';
$string['extension_mins']  = 'Przedłużenie (min)';
$string['endtime'] = 'Czas końcowy';
$string['extendtimeby']     = 'Przedłuż czas o';
$string['extendtime']       = 'Przedłuż czas';
$string['addnote']          = 'Dodaj notatkę';
$string['currenttime']      = 'Aktualny czas:';
$string['start']     		= 'Start'; //cognate
$string['end']      		= 'Koniec';
$string['start_but'] = 'Rozpocznij'; //cognate
$string['endat_but']  = 'Zakończ o';
$string['session_end'] 		= 'Koniec sesji';
$string['duration'] = 'Czas trwania';
$string['mins'] = 'min.';
$string['papernote'] = 'Uwagi dot. arkusza';
$string['extratime'] = 'Dodatkowy czas';
$string['checklist'] = '<div><strong>Lista zadań</strong></div>
    <div><em>Przed egzaminem</em></div>
    <ol>
    <li>Rozłóż instrukcje logowania przy każdym komputerze</li>
    <li>Umieść przy każdym komputerze czystą kartkę papieru</li>
    <li>Sprawdź czy każdy student zalogował się poprawnie</li>
    <li>Użyj konta \'gość\' dla każdego studenta, który nie mógł się zalogować</li>
    <li><strong>Uwaga:</strong> Nie rozpoczynaj przed zaplanowanym czasem startu</li>
    </ol>

    <div><em>W czasie egzaminu</em></div>
    <ol>
    <li>Rejestruj drobne problemy w plikach studentów (<a href="%shelp.html" target="_blank">przykładowe problemy</a>)</li>
    <li>Rejestruj problemy z treściami arkuszy/pytań</li>
    <li>Zadzwoń na poniższy numer w razie większych kłopotów</li>
    </ol>

    <div><em>Po egzaminie</em></div>
    <ol start="4">
    <li>"To już koniec egzaminu. Proszę przejść do ostatniego ekranu i kliknąć na \'Zakończ\'."</li>
    <li>"Proszę kliknąć \'Zamknij okno\' a następnie nacisnąć równocześnie CTRL, ALT i DELETE aby się wylogować."</li>
    <li>Zbierz instrukcje logowania</li>
    <li>Zbierz pozostałe czyste kartki</li>
    <li>Upewnij się, że wylogowano się ze <strong>wszystkich</strong> komputerów</li>
    </ol>'; //??
$string['time'] = 'Czas';
$string['timedexam'] = 'Egzamin odmierzany czasowo:';
$string['timeerror'] = 'Błąd czasu'; 
$string['timeerrormsg'] = 'Egzamin musi trwać co najmniej %d minut'; 
$string['examquestionclarifications'] = 'Wyjaśnienie pytania egzaminacyjnego';
?>