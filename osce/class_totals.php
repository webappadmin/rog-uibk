<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Simon Wilkinson
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/

require '../include/staff_auth.inc';

require_once '../include/demo_replace.inc';
require_once '../include/errors.inc';
require_once '../include/sort.inc';
require_once './osce.inc';

require_once '../classes/class_totals.class.php';
require_once '../classes/paperutils.class.php';
require_once '../classes/paperproperties.class.php';
require_once '../classes/results_cache.class.php';

$demo = is_demo($userObject);

$paperID   = check_var('paperID', 'GET', true, false, true);
$startdate = check_var('startdate', 'GET', true, false, true);
$enddate   = check_var('enddate', 'GET', true, false, true);

$percent      = (isset($_GET['percent'])) ? $_GET['percent'] : 100;
$ordering     = (isset($_GET['ordering'])) ? $_GET['ordering'] : 'asc';
$absent       = (isset($_GET['absent'])) ? $_GET['absent'] : 0;
$sortby       = (isset($_GET['sortby'])) ? $_GET['sortby'] : 'name';
$studentsonly = (isset($_GET['studentsonly'])) ? $_GET['studentsonly'] : 1;
$repcourse    = (isset($_GET['repcourse'])) ? $_GET['repcourse'] : '%';
$repmodule    = (isset($_GET['repmodule'])) ? $_GET['repmodule'] : '';

// Get some paper properties
$propertyObj = PaperProperties::get_paper_properties_by_id($paperID, $mysqli, $string);

$paper = $propertyObj->get_paper_title();
$crypt_name = $propertyObj->get_crypt_name();

$exclusions = new Exclusion($paperID, $mysqli);
$exclusions->load();                                                                                  // Get any questions to exclude.

$report = new ClassTotals($studentsonly, $percent, $ordering, $absent, $sortby, $userObject, $propertyObj, $startdate, $enddate, $repcourse, $repmodule, $mysqli);
$report->load_answers();
$paper_buffer = $report->get_paper_buffer();
$question_no  = $report->get_question_no();

$user_results = load_osce_results($propertyObj, $demo, $configObject, $question_no, $mysqli);
$report->set_user_results($user_results);
$report->generate_stats();
$user_no = $report->get_user_no();

$q_medians = load_osce_medians($mysqli);

if ($propertyObj->get_pass_mark() == 101) {
  $borderline_method = true;
} else {
  $borderline_method = false;
}

if ($borderline_method) {
  $passmark = getBlinePassmk($user_results, $user_no, $propertyObj);
} elseif ($propertyObj->get_pass_mark() == 102) {
  $passmark = 'N/A';
} else {
  $passmark = $propertyObj->get_pass_mark();
}
$distinction_mark = $propertyObj->get_distinction_mark();


set_classification($user_results, $passmark, $user_no, $string);
$report->sort_results();
$user_results = array_csort($user_results, $sortby, $ordering);

$completed_no = 0;
$total_score = 0;
$classifications = array(''=>'', 1=>0, 2=>0, 3=>0, 4=>0, 5=>0, 'ERROR'=>0);

for ($i=0; $i<$user_no; $i++) {
  if ($user_results[$i]['metadataID'] != '') {   // No attendance
    $classifications[$user_results[$i]['rating']]++;
    $total_score += $user_results[$i]['mark'];
    $completed_no++;
  }
}

$stats = $report->get_stats();                        // Generate the main statistics

$results_cache = new ResultsCache($mysqli);
if ($results_cache->should_cache($propertyObj, $percent, $absent)) {
  $results_cache->save_paper_cache($paperID, $percent, $absent, $stats);                  // Cache general paper stats
  
  $results_cache->save_student_mark_cache($paperID, $percent, $absent, $user_results);    // Cache student/paper marks
  
  $results_cache->save_median_question_marks($paperID, $percent, $absent, $q_medians);    // Cache the question/paper medians
}

rating_num_text($user_results, $user_no, $propertyObj, $string);
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=<?php echo $configObject->get('cfg_page_charset') ?>" />
  
  <title>Rog&#333;: <?php echo $string['classtotals'] . ' ' . $configObject->get('cfg_install_type'); ?></title>
  
  <link rel="stylesheet" type="text/css" href="../css/body.css" />
  <link rel="stylesheet" type="text/css" href="../css/header.css" />
  <link rel="stylesheet" type="text/css" href="../css/class_totals.css" />
  <link rel="stylesheet" type="text/css" href="../css/warnings.css" />
  
  <script type="text/javascript" src="../js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="../js/staff_help.js"></script>
  <script type="text/javascript" src="../js/popup_menu.js"></script>
  <script type="text/javascript" src="../js/toprightmenu.js"></script>
  <script language="JavaScript">    
    function setVars(metadataID, currentUserID) {
      $('#metadataID').val(metadataID);
      $('#userID').val(currentUserID);
    }
    
    function viewScript() {
      $('#menudiv').hide();
      var winwidth = 750;
      var winheight = screen.height-80;
      window.open("view_form.php?paperID=<?php echo $paperID; ?>&userID=" + $('#userID').val() + "","paper","width="+winwidth+",height="+winheight+",left=30,top=20,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
    }
    
    function viewFeedback() {
      $('#menudiv').hide();
      var winwidth = screen.width-80;
      var winheight = screen.height-80;
      window.open("../students/objectives_feedback.php?id=<?php echo $crypt_name; ?>&userID=" + $('#userID').val() + "&metadataID=" + $('#metadataID').val() + "","feedback","width="+winwidth+",height="+winheight+",left=30,top=20,scrollbars=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,resizable");
    }
    
    function viewProfile() {
      $('#menudiv').hide();
      window.top.location = '../users/details.php?userID=' + $('#userID').val();
    }
    
    document.onmousedown = mouseSelect;
  </script>
</head>

<body>
<?php
require '../include/toprightmenu.inc';
	
echo draw_toprightmenu();

$popup_width = 180;
if ($language != 'en') {
  $popup_width = 300;
}
?>
<div id="menudiv" class="popupmenu" style="width:<?php echo $popup_width; ?>px" onmouseover="javascript:overpopupmenu=true;" onmouseout="javascript:overpopupmenu=false;">
<table cellspacing="2" cellpadding="0" border="0" style="font-size:100%; background-color:white; width:100%">
  <tr><td>
    <table cellspacing="0" cellpadding="1" border="0" style="font-size:90%; background-color:white; width:100%">
      <tr>
        <td id="item1a" style="text-align:center; background-color:#F1F5FB; width:24px" onmouseover="menuRowOn('1');" onmouseout="menuRowOff('1');" onclick="viewScript();"><img src="../artwork/osce_16.gif" width="16" height="16" alt="" /></td><td id="item1b" style="padding-left:8px; background-color:#FFFFFF; cursor:default" onmouseover="menuRowOn('1');" onmouseout="menuRowOff('1');" onclick="viewScript();"><?php echo $string['oscemarksheet']; ?></td>
      </tr>
      <tr>
        <td id="item2a" style="text-align:center; background-color:#F1F5FB; width:24px" onmouseover="menuRowOn('2');" onmouseout="menuRowOff('2');" onclick="viewFeedback();"><img src="../artwork/ok_comment.png" width="16" height="16" alt="" /></td><td id="item2b" style="padding-left:8px; background-color:#FFFFFF; cursor:default" onmouseover="menuRowOn('2');" onmouseout="menuRowOff('2');" onclick="viewFeedback();"><?php echo $string['feedback']; ?></td>
      </tr>
      <tr>
        <td style="background-color:#F1F5FB; width:22px"> </td><td style="padding-left:8px; text-align:right"><img src="../artwork/popup_divider.png" width="100%" height="3" alt="-" /></td>
      </tr>
      <tr>
        <td id="item3a" style="text-align:center; background-color:#F1F5FB; width:24px" onmouseover="menuRowOn('3');" onmouseout="menuRowOff('3');" onclick="viewProfile();"><img src="../artwork/small_user_icon.gif" width="16" height="16" alt="" /></td><td id="item3b" style="padding-left:8px; background-color:#FFFFFF; cursor:default" onmouseover="menuRowOn('3');" onmouseout="menuRowOff('3');" onclick="viewProfile();"><?php echo $string['studentprofile']; ?></td>
      </tr>
    </table>
  </td></tr>
</table>
</div>

<?php
  //output table heading
  if ($borderline_method) {
    $table_order = array(''=>'', $string['name']=>'name', $string['studentid']=>'student_id', $string['course']=>'grade', $string['total']=>'numeric_score', $string['rating']=>'rating', $string['classification']=>'classification', $string['starttime']=>'started', $string['examiner']=>'examiner');
  } else {
    $table_order = array(''=>'', $string['name']=>'name', $string['studentid']=>'student_id', $string['course']=>'grade', $string['total']=>'numeric_score', $string['classification']=>'classification', $string['starttime']=>'started', $string['examiner']=>'examiner');
  }
  $metadata_cols = array();
  if (isset($user_results[0])){
    foreach ($user_results[0] as $key => $val) {
      if (strrpos($key,'meta_') !== false) {
        $key_display = ucfirst(str_replace('meta_','',$key));
        $table_order[$key_display] = $key;
        $metadata_cols[$key] = $key;
      }
    }
  }
  
  $column_no = count($table_order) + count($metadata_cols);

  echo "<table class=\"header\" style=\"font-size:80%\">\n";
  echo "<tr><th class=\"h\" colspan=\"" . ($column_no - 1) . "\">";
  if (isset($_GET['repmodule']) and $_GET['repmodule'] != '') {
    $report_title = sprintf($string['classtotalsmodule'], $_GET['repmodule']);
  } else {
    $report_title = $string['classtotals'];
  }

  echo '<div class="breadcrumb"><a href="../staff/index.php">' . $string['home'] . '</a>';
  if (isset($_GET['folder']) and $_GET['folder'] != '') {
    echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../folder/details.php?folder=' . $_GET['folder'] . '">' . folder_utils::get_folder_name($_GET['folder'], $mysqli) . '</a>';
  } elseif (isset($_GET['module']) and $_GET['module'] != '') {
    echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../folder/details.php?module=' . $_GET['module'] . '">' . module_utils::get_moduleid_from_id($_GET['module'], $mysqli) . '</a>';
  }
  echo '&nbsp;&nbsp;<img src="../artwork/breadcrumb_arrow.png" width="4" height="7" alt="-" />&nbsp;&nbsp;<a href="../paper/details.php?paperID=' . $_GET['paperID'] . '">' . $paper . '</a></div>';
  
  echo "<span style=\"margin-left:10px; font-size:200%; color:black; font-weight:bold\">$report_title</span></th><th class=\"h\" style=\"text-align:right; vertical-align:top\"><img src=\"../artwork/toprightmenu.gif\" id=\"toprightmenu_icon\"></th></tr>\n";

  if (isset($_GET['folder'])) {
    $tmp_folder = '&folder=' . $_GET['folder'];
  } else {
    $tmp_folder = '';
  }

  if (isset($_GET['module'])) {
    $tmp_module = '&module=' . $_GET['module'];
  } else {
    $tmp_module = '';
  }
  
  // output table header
  if (isset($user_results[0])) {
    echo "<tr style=\"font-size:110%\">\n";
    foreach ($table_order as $display => $key) {
      if ($key == '') {
        echo "<th>";
      } else {
        echo "<th class=\"vert_div\">";
      }
      if ($sortby == $key and $ordering == 'asc') {
        echo "<a style=\"color:black\" href=\"" . $_SERVER['PHP_SELF'] . "?paperID=" . $_GET['paperID'] . "&repmodule=" . $_GET['repmodule'] . "&repcourse=" . $_GET['repcourse'] . $tmp_module . $tmp_folder . "&startdate=$startdate&enddate=$enddate&sortby=$key&ordering=desc&percent=$percent&absent=$absent\">$display</a>&nbsp;<img src=\"../artwork/desc.gif\" width=\"9\" height=\"7\" /></th>";
      } elseif ($sortby == $key and $ordering == 'desc') {
        echo "<a style=\"color:black\" href=\"" . $_SERVER['PHP_SELF'] . "?paperID=" . $_GET['paperID'] . "&repmodule=" . $_GET['repmodule'] . "&repcourse=" . $_GET['repcourse'] . $tmp_module . $tmp_folder . "&startdate=$startdate&enddate=$enddate&sortby=$key&ordering=asc&percent=$percent&absent=$absent\">$display</a>&nbsp;<img src=\"../artwork/asc.gif\" width=\"9\" height=\"7\" /></th>";
      } else {
        echo "<a style=\"color:black\" href=\"" . $_SERVER['PHP_SELF'] . "?paperID=" . $_GET['paperID'] . "&repmodule=" . $_GET['repmodule'] . "&repcourse=" . $_GET['repcourse'] . $tmp_module . $tmp_folder . "&startdate=$startdate&enddate=$enddate&sortby=$key&ordering=asc&percent=$percent&absent=$absent\">$display</a></th>";
      }
    }
    echo "</tr>\n";
  }
	if ($user_no == 0) {
    echo "</table>\n<table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" style=\"margin: 0px auto; width:75%; border: 1px solid #C0C0C0; text-align:left; font-size:85%\">\n<tr><td colspan=\"2\" style=\"background-color:#F2B100; height:3px\"> </td></tr>\n<tr><td style=\"width:16px; padding-top:5px; padding-bottom:5px\"><img src=\"../artwork/information_icon.gif\" width=\"16\" height=\"16\" alt=\"i\" border=\"0\" /></td><td style=\"padding-top:5px; padding-bottom:5px\">&nbsp;" . sprintf($string['noattempts'], $report->nicedate($_GET['startdate']), $report->nicedate($_GET['enddate'])) . "</td></tr></table>\n<div>\n</body>\n</html>";
    exit;
	}
 
  for ($i=0; $i<$user_no; $i++) {
    if ($user_results[$i]['started'] == '') {   // No attendance
      echo "<tr class=\"nonattend\"><td>&nbsp;</td><td>&nbsp;<a class=\"user\" href=\"../users/details.php?userID=" . $user_results[$i]['userID'] . "\">" . $user_results[$i]['display_name'] . "</a></td><td>&nbsp;" . $user_results[$i]['student_id'] . "</td><td colspan=\"" . ($column_no - 2) . "\" style=\"text-align:center\">&lt;" . $string['noattendance'] . "&gt;</td></tr>\n";
    } else {
      echo "<tr><td class=\"greyln\"><img src=\"../artwork/osce_16.gif\" style=\"cursor:hand\" onclick=\"ItemSelMenu('" . $user_results[$i]['userID'] . "', event);\" width=\"16\" height=\"16\" /></td>";
      echo '<td class="greyln';
      if ($sortby == 'name') echo ' ordered';
      echo "\">&nbsp;<span style=\"cursor:hand\" onclick=\"popMenu(3, event); setVars('" . $user_results[$i]['metadataID'] . "', '" . $user_results[$i]['userID'] . "');\">" . $user_results[$i]['title'] . " " . $user_results[$i]['surname'] . ", <span style=\"color:#808080\">" . $user_results[$i]['first_names'] . "</span></td>";
      echo '<td class="greyln';
      if ($sortby == 'student_id') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['student_id'] . "</td>";
      echo '<td class="greyln';
      if ($sortby == 'grade') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['grade'] . "</td>";
      echo '<td class="greyln';
      if ($sortby == 'numeric_score') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['mark'] . "</td>";
            
      if ($borderline_method) {
        echo '<td class="greyln';
        if ($sortby == 'rating') echo ' ordered';
        echo "\">&nbsp;" . $user_results[$i]['rating'] . "</td>\n";
      }
      
      echo '<td class="greyln';
      if ($sortby == 'classification') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['classification'] . "</td>";
      echo '<td class="greyln';
      if ($sortby == 'started') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['display_started'] . "</td>\n";
      echo '<td class="greyln';
      if ($sortby == 'examiner') echo ' ordered';
      echo "\">&nbsp;" . $user_results[$i]['examiner'] . "</td></tr>\n";
    }
  }

  echo "<tr><td colspan=\"" . $column_no . "\">&nbsp;</td></tr>\n";
  echo "<tr><td colspan=\"" . $column_no . "\"><table border=\"0\" style=\"padding-left:10px; padding-right:2px; padding-bottom:5px; width:100%; color:#1E3287\"><tr><td>" . $string['summary'] . "</td><td style=\"width:98%\"><hr noshade=\"noshade\" style=\"border:0px; height:1px; color:#E5E5E5; background-color:#E5E5E5; width:100%\" /></td></tr></table></td></tr>\n";
  
  echo "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"font-size:80%; line-height:150%\">\n";
  echo "<tr><td align=\"right\" style=\"width:110px\">" . $string['cohortsize'] . "</td><td style=\"text-align:right; width:40px\">" . $user_no . "</td></tr>\n";
  
  if ($borderline_method) {
    echo "<tr><td align=\"right\">" . $string['passmark'] . "</td><td style=\"text-align:right\">" . round($passmark, 2) . "</td><td>% (" . $string['borderlinemethod'] . ")</td></tr>\n";
  } elseif ($propertyObj->get_pass_mark() != 102) {  // Not the N/A option
    echo "<tr><td align=\"right\">" . $string['passmark'] . "</td><td style=\"text-align:right\">" . $propertyObj->get_pass_mark() . "</td><td>%</td></tr>\n";
  }
  
  $labels = get_labels($propertyObj);
  foreach ($labels as $i => $label) {
    echo "<tr><td align=\"right\">" . $string[strtolower($label)] . "</td><td style=\"text-align:right\">" . $classifications[$i] . "</td></tr>\n";
  }
  echo "</table>\n";
  
  echo "</td></tr>\n";
  echo "</table>\n";
  
  $mysqli->close();
?>
<input type="hidden" id="userID" value="" />
<input type="hidden" id="metadataID" value="" />
</body>
</html>
