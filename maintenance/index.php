<?php
// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
* 
* @author Rob Ingram
* @version 1.0
* @copyright Copyright (c) 2014 The University of Nottingham
* @package
*/
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  
  <title>Undergoing Maintenance</title>

  <link rel="stylesheet" href="../css/body.css" type="text/css" />
  <link rel="stylesheet" href="../css/screen.css" type="text/css" />
  <style type="text/css">
    body {font-size:90%; margin-top:10px}
    .field {padding-top:4px; padding-left:6px; font-weight:bold}
  </style>
</head>

<body>
	<div align="center">
  	<table cellpadding="0" cellspacing="0" style="width:500px; border:1px #C00000 solid">
    	<tr style="height:70px; width:100%; color:#C00000; font-size:150%; font-weight:bold; padding-left:6px"><td style="text-align:right; width:135px"><img src="artwork/exclamation_48.png" width="48" height="48" alt="Under maintenance" /></td><td style="text-align:left">&nbsp;&nbsp;Undergoing Maintenance</td></tr>
    	<tr><td colspan="2" style="padding-top:4px; padding-left:6px; background-color:#FFDDDD"><p>Rog&#333; is currently undergoing routine maintenance. Please try again later.</p></td></tr>
    </table>
  </div>
</body>
</html>